#-------------------------------------------------
#
# Project created by QtCreator 2015-08-03T18:03:48
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = task
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    DB.cpp

HEADERS += \
    MyObject.h \
    DB.h
