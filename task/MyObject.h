#ifndef MYOBJECT_H
#define MYOBJECT_H

#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <QEvent>
#include <QObject>

class MyObject : public QObject
{
    Q_OBJECT

public:
    MyObject(QObject *parent = 0);
    ~MyObject();
    void intervalTask();
    void dingQiTask();
    void updateTask(int timerId);

    QString m_curDir;
    QString m_webroot;

    QMap<int,QString>m_taskId;
    QMap<int,QString>m_taskUrl;

protected:
    void timerEvent(QTimerEvent *event);
};

#endif // MYOBJECT_H
