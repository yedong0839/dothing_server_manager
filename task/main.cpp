﻿#include "MyObject.h"
#include "DB.h"
#include <QDateTime>
#include <QFile>
#include <QProcess>
#include <QSqlError>

void outputMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static QMutex mutex;
    mutex.lock();

    bool is_collect_log = false;
    QString text;
    switch (type)
    {
    case QtDebugMsg:

        if (msg.startsWith("sys||"))//sys||信息
        {
            is_collect_log = true;
            QStringList info = msg.split("||");
            text = info.at(1);
        }
        else
        {
            text = QString("Debug:");
        }
        break;

    case QtWarningMsg:
        text = QString("Warning:");
        break;

    case QtCriticalMsg:
        text = QString("Critical:");
        break;

    case QtFatalMsg:
        text = QString("Fatal:");
    }

    if (is_collect_log)
    {
        QFile file("syslog.txt");
        file.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream text_stream(&file);
        text_stream << text.mid(2, text.length() - 3) << "\r\n";
        file.flush();
        file.close();
    }
    else if (text != "Warning")
    {
        QString current_date_time = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
        QString current_date = QString("(%1)").arg(current_date_time);
        QString context_info = QString("File:(%1) Line:(%2)").arg(QString(context.file)).arg(context.line);
        QString message = QString("%1 %2 %3 %4").arg(text).arg(context_info).arg(msg).arg(current_date);
        QFile file("tasklog.txt");

        file.open(QIODevice::WriteOnly | QIODevice::Append);
        QTextStream text_stream(&file);
        text_stream << message << "\r\n";
        file.flush();
        file.close();
    }
    mutex.unlock();
}

MyObject::MyObject(QObject *parent)
    : QObject(parent)
{
//    QSettings settings("data.ini", QSettings::IniFormat);
//    DBSetting dbSetting;
//    dbSetting.server = settings.value("data/server","localhost").toString();
//    dbSetting.port = settings.value("data/port","3336").toString();
//    dbSetting.userName = settings.value("data/userName","root").toString();
//    dbSetting.pwd = settings.value("data/pwd","myoa888").toString();
//    dbSetting.database = settings.value("data/database","td_oa").toString();

//    DB::setDbSetting(dbSetting);

    //DB::dbSetting = dbSetting;
//    DB::m_server = "localhost";
//    DB::setDbSetting();

#ifdef _DEBUG
    m_curDir = "D:/tools/dothing_server_manager";
#else
    m_curDir = QCoreApplication::applicationDirPath();
#endif

    dingQiTask();
    intervalTask();

}

MyObject::~MyObject(){
//    delete DB::getInstance();
    //deleteLater();
}

void MyObject::timerEvent(QTimerEvent *event)
{
    qDebug() << "Timer ID:" << event->timerId();

    QString task_file = m_taskUrl.value(event->timerId());
    QString cmd = QString("%1/server/php/php.exe %2").arg(m_curDir).arg(task_file);

    QProcess p(0);
    p.start(cmd);
    p.waitForStarted();
    p.waitForFinished();
    qDebug()<<cmd;
    qDebug()<<QString::fromLocal8Bit(p.readAllStandardError());
    updateTask(event->timerId());

}
/**
 * 定期任务
 */
void MyObject::dingQiTask(){

}

/**
 *间隔任务
 * @brief MyObject::intervalTask
 */
void MyObject::intervalTask(){

    DB db;
    QSqlQuery query = db.getQuery();

    //间隔任务
    QString sql = "SELECT `interval`,task_url,task_id from OFFICE_TASK where (TASK_TYPE='0' or TASK_TYPE='3') and use_flag=1 order by TASK_ID";
    if (!query.exec(sql))
    {
        qDebug() << "sql语句：" + sql;
        return;
    }
    QString task_url,task_id;
    int timer_id,interval;
    while (query.next()){
        interval = query.value(0).toInt();
        task_url = query.value(1).toString();
        task_id = query.value(2).toString();
        if(interval == 0){
            qDebug() << "task_id->"+task_id+"interval time does not set";
            continue;
        }
        if(task_url == ""){
            qDebug() << "task_id->"+task_id+" task_url is null";
            continue;
        }
        QFile task_file(m_webroot+task_url);
        if(!task_file.exists()){
            qDebug() << "task_id->"+task_id+" task file("+m_webroot+task_url+") does not exits";
            continue;
        }

        timer_id = startTimer(1000*60*interval);
        m_taskId.insert(timer_id,task_id);
        m_taskUrl.insert(timer_id,task_url);
    }
}

void MyObject::updateTask(int timerId){

    DB db;
    QSqlQuery query = db.getQuery();
//    QSqlQuery query = DB::getInstance()->getQuery();

    QString taskId = m_taskId.value(timerId);

    QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
    QString curTime = time.toString("yyyy-MM-dd hh:mm:ss"); //设置显示格式

    QString sql = QString("update OFFICE_TASK set last_exec ='%1' where task_id=%2").arg(curTime).arg(taskId);

    if (!query.exec(sql))
    {
        QString error = query.lastError().text();
        qDebug() << "sql exec error->" + sql+"->"+error;
    }

}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(outputMessage);
    QFile file("tasklog.txt");
    file.remove();
    QCoreApplication a(argc, argv);
    MyObject my;
    return a.exec();
}
