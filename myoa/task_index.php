<?php
//zend53   
//Decode by www.dephp.cn  QQ 2859470
?>
<?php

include_once ("inc/auth.inc.php");
$HTML_PAGE_TITLE = _("定时任务管理");
include_once ("inc/header.inc.php");
echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"";
echo MYOA_STATIC_SERVER;
echo "/static/theme/";
echo $_SESSION["LOGIN_THEME"];
echo "/dialog.css\">\r\n<script src=\"";
echo MYOA_JS_SERVER;
echo "/static/js/dialog.js\"></script>\r\n<script type=\"text/javascript\" src=\"";
echo MYOA_JS_SERVER;
echo "/static/js/utility.js\"></script>\r\n<script Language=\"JavaScript\">\r\nfunction exec_task(TASK_ID)\r\n{\r\n   if(window.confirm(\"";
echo _("确定要立即执行该任务吗？");
echo "\"))\r\n   {\r\n      ShowDialog('comment');\r\n      _get(\"task.php\", \"TASK_ID=\"+TASK_ID, load_task);\r\n   }\r\n}\r\n\r\nfunction load_task(req)\r\n{\r\n   if(req.status == 200 && req.responseText.substr(0, 3).toUpperCase() == \"+OK\")\r\n   {\r\n      _get(req.responseText.substr(4), \"\", show_task);\r\n   }\r\n   else\r\n   {\r\n      HideDialog('comment');\r\n      if(req.status == 200)\r\n         alert(req.responseText);\r\n      else\r\n         alert(\"";
echo _("HTTP错误");
echo " \" + req.status);\r\n   }\r\n}\r\n\r\nfunction show_task(req)\r\n{\r\n   HideDialog('comment');\r\n   if(req.status == 200 && req.responseText.substr(0, 3).toUpperCase() == \"+OK\")\r\n   {\r\n      alert(\"";
echo _("任务执行完成");
echo "\");\r\n   }\r\n   else\r\n   {\r\n      if(req.status == 200)\r\n         alert(req.responseText);\r\n      else\r\n         alert(\"";
echo _("HTTP错误");
echo " \" + req.status);\r\n   }\r\n}\r\n\r\nfunction delete_task(TASK_ID)\r\n{\r\n   if(window.confirm(\"";
echo _("确定要删除该任务吗？删除后将无法恢复。");
echo "\"))\r\n      window.location = \"delete.php?TASK_ID=\" + TASK_ID;\r\n}\r\n</script>\r\n\r\n<body class=\"bodycolor\">\r\n\r\n<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"3\" class=\"small\">\r\n  <tr>\r\n    <td class=\"Big\"><img src=\"";
echo MYOA_STATIC_SERVER;
echo "/static/images/task.gif\" align=\"absmiddle\"><span class=\"big3\"> <a name=\"bottom\">";
echo _("间隔执行任务");
echo "</span>\r\n    </td>\r\n  </tr>\r\n</table>\r\n<table class=\"TableList\" width=\"95%\" style=\"min-width:1044px;\" align=\"center\">\r\n  <thead class=\"TableHeader\" align=\"center\">\r\n    <td>";
echo _("任务名称");
echo "</td>\r\n    <td>";
echo _("任务描述");
echo "</td>\r\n    <td width=\"120\">";
echo _("执行间隔(分钟)");
echo "</td>\r\n    <td width=\"120\">";
echo _("上次执行时间");
echo "</td>\r\n    <td width=\"120\">";
echo _("上次执行结果");
echo "</td>\r\n    <td width=\"50\">";
echo _("状态");
echo "</td>\r\n    <td width=\"150\">";
echo _("操作");
echo "</td>\r\n  </thead>\r\n";

if ($connstatus == 1) {
	$connstatus = true;
}
else {
	$connstatus = "";
}

$query = "SELECT * from OFFICE_TASK where TASK_TYPE='0' or TASK_TYPE='3' order by TASK_ID";
$cursor = exequery(TD::conn(), $query, $connstatus);

while ($ROW = mysql_fetch_array($cursor)) {
	$TASK_ID = $ROW["TASK_ID"];
	$TASK_TYPE = $ROW["TASK_TYPE"];
	$INTERVAL = $ROW["INTERVAL"];
	$LAST_EXEC = $ROW["LAST_EXEC"];
	$TASK_NAME = $ROW["TASK_NAME"];
	$TASK_DESC = $ROW["TASK_DESC"];
	$USE_FLAG = $ROW["USE_FLAG"];
	$SYS_TASK = $ROW["SYS_TASK"];
	$EXEC_FLAG = $ROW["EXEC_FLAG"];
	$EXEC_MSG = $ROW["EXEC_MSG"];

	if ($EXEC_FLAG == "0") {
		$EXEC_FLAG = _("未执行");
	}
	else if ($EXEC_FLAG == "1") {
		$EXEC_FLAG = _("成功");
	}
	else if ($EXEC_FLAG == "2") {
		$EXEC_FLAG = _("失败");
	}

	$EXEC_MSG = htmlspecialchars($EXEC_MSG);
	echo "  <tr class=\"TableData\">\r\n    <td align=\"center\">";
	echo $TASK_NAME;
	echo "</td>\r\n    <td>";
	echo $TASK_DESC;
	echo "</td>\r\n    <td align=\"center\">";
	echo $INTERVAL;
	echo "</td>\r\n    <td align=\"center\">";
	echo $LAST_EXEC;
	echo "</td>\r\n    <td align=\"center\" title=\"";
	echo $EXEC_MSG;
	echo "\">";
	echo $EXEC_FLAG;
	echo "</td>\r\n    <td align=\"center\" title=\"";
	echo $USE_FLAG == "1" ? _("启用") : _("停用");
	echo "\"><img src=\"";
	echo MYOA_STATIC_SERVER;
	echo "/static/images/";
	echo $USE_FLAG == "1" ? "correct" : "error";
	echo ".gif\"></td>\r\n    <td>&nbsp;\r\n      <a href=\"edit.php?TASK_ID=";
	echo $TASK_ID;
	echo "&IS_MAIN=";
	echo $IS_MAIN;
	echo "\"> ";
	echo _("编辑");
	echo "</a>\r\n";

	if ($SYS_TASK != "1") {
		echo "      <a href=\"javascript:delete_task('";
		echo $TASK_ID;
		echo "');\"> ";
		echo _("删除");
		echo "</a>\r\n";
	}

	if (($USE_FLAG == "1") && ($TASK_TYPE == "0")) {
		echo "      <a href=\"javascript:exec_task('";
		echo $TASK_ID;
		echo "');\"> ";
		echo _("立即执行");
		echo "</a>\r\n";
	}

	echo "    </td>\r\n  </tr>\r\n";
}

echo "</table>\r\n\r\n<br>\r\n<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"3\">\r\n <tr>\r\n   <td background=\"";
echo MYOA_STATIC_SERVER;
echo "/static/images/dian1.gif\" width=\"100%\"></td>\r\n </tr>\r\n</table>\r\n \r\n\r\n<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"3\" class=\"small\">\r\n  <tr>\r\n    <td class=\"Big\"><img src=\"";
echo MYOA_STATIC_SERVER;
echo "/static/images/task.gif\" align=\"absmiddle\"><span class=\"big3\"> <a name=\"bottom\">";
echo _("定点执行任务");
echo "</span>\r\n    </td>\r\n  </tr>\r\n</table>\r\n<table class=\"TableList\" width=\"95%\" style=\"min-width:1044px;\" align=\"center\">\r\n  <thead class=\"TableHeader\" align=\"center\">\r\n    <td>";
echo _("任务名称");
echo "</td>\r\n    <td>";
echo _("任务描述");
echo "</td>\r\n    <td width=\"120\">";
echo _("执行间隔(天)");
echo "</td>\r\n    <td width=\"100\">";
echo _("执行时间");
echo "</td>\r\n    <td width=\"100\">";
echo _("上次执行");
echo "</td>\r\n    <td width=\"120\">";
echo _("上次执行结果");
echo "</td>\r\n    <td width=\"50\">";
echo _("状态");
echo "</td>\r\n    <td width=\"150\">";
echo _("操作");
echo "</td>\r\n  </thead>\r\n";
$query = "SELECT * from OFFICE_TASK where TASK_TYPE='1' or TASK_TYPE='2' order by TASK_ID";
$cursor = exequery(TD::conn(), $query, $connstatus);

while ($ROW = mysql_fetch_array($cursor)) {
	$TASK_ID = $ROW["TASK_ID"];
	$TASK_TYPE = $ROW["TASK_TYPE"];
	$INTERVAL = $ROW["INTERVAL"];
	$EXEC_TIME = $ROW["EXEC_TIME"];
	$LAST_EXEC = $ROW["LAST_EXEC"];
	$TASK_NAME = $ROW["TASK_NAME"];
	$TASK_DESC = $ROW["TASK_DESC"];
	$TASK_CODE = $ROW["TASK_CODE"];
	$USE_FLAG = $ROW["USE_FLAG"];
	$SYS_TASK = $ROW["SYS_TASK"];
	$EXEC_FLAG = $ROW["EXEC_FLAG"];
	$EXEC_MSG = $ROW["EXEC_MSG"];

	if ($EXEC_FLAG == "0") {
		$EXEC_FLAG = _("未执行");
	}
	else if ($EXEC_FLAG == "1") {
		$EXEC_FLAG = _("成功");
	}
	else if ($EXEC_FLAG == "2") {
		$EXEC_FLAG = _("失败");
	}

	$EXEC_MSG = htmlspecialchars($EXEC_MSG);
	echo "  <tr class=\"TableData\">\r\n    <td align=\"center\">";
	echo $TASK_NAME;
	echo "</td>\r\n    <td>";
	echo $TASK_DESC;
	echo "</td>\r\n    <td align=\"center\">";
	echo $INTERVAL;
	echo "</td>\r\n    <td align=\"center\">";
	echo $EXEC_TIME;
	echo "</td>\r\n    <td align=\"center\">";
	echo $LAST_EXEC;
	echo "</td>\r\n    <td align=\"center\" title=\"";
	echo $EXEC_MSG;
	echo "\">";
	echo $EXEC_FLAG;
	echo "</td>\r\n    <td align=\"center\" title=\"";
	echo $USE_FLAG == "1" ? _("启用") : _("停用");
	echo "\"><img src=\"";
	echo MYOA_STATIC_SERVER;
	echo "/static/images/";
	echo $USE_FLAG == "1" ? "correct" : "error";
	echo ".gif\"></td>\r\n    <td>&nbsp;\r\n      <a href=\"edit.php?TASK_ID=";
	echo $TASK_ID;
	echo "\"> ";
	echo _("编辑");
	echo "</a>\r\n";

	if ($SYS_TASK != "1") {
		echo "      <a href=\"javascript:delete_task('";
		echo $TASK_ID;
		echo "');\"> ";
		echo _("删除");
		echo "</a>\r\n";
	}

	if (($USE_FLAG == "1") && ($TASK_TYPE == "1")) {
		echo "      <a href=\"javascript:exec_task('";
		echo $TASK_ID;
		echo "');\"> ";
		echo _("立即执行");
		echo "</a>\r\n";
	}

	if (($TASK_CODE == "clean") || ($TASK_CODE == "check_sp") || ($TASK_CODE == "office_auto")) {
		echo "      <a href=\"";
		echo $TASK_CODE;
		echo "\"> ";
		echo _("设置");
		echo "</a>\r\n";
	}
	else if (substr($TASK_CODE, 0, 9) == "db_backup") {
		echo "      <a href=\"javascript:alert('";
		echo _("请到[系统管理->数据库管理->数据库热备份]中进行设置");
		echo "');\"> ";
		echo _("设置");
		echo "</a>\r\n";
	}

	echo "    </td>\r\n  </tr>\r\n";
}

echo "</table>\r\n\r\n<div id=\"overlay\"></div>\r\n<div id=\"comment\" class=\"ModalDialog\" style=\"width:200px;\">\r\n  <div id=\"detail_body\" class=\"body\" style=\"text-align:center;\">\r\n     <img src=\"";
echo MYOA_STATIC_SERVER;
echo "/static/images/loading.gif\"><br><br>\r\n     ";
echo _("任务正在执行，请稍候...");
echo "<br><br>\r\n  </div>\r\n</div>\r\n</body>\r\n</html>";

?>
