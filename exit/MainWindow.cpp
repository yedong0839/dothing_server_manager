#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QProcess>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	connect(this,SIGNAL(runSignal()),this,SLOT(run()));
	QObject::connect(this, SIGNAL(runOverSignal()), QApplication::instance(), SLOT(quit()), Qt::QueuedConnection);
	emit runSignal();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::run()
{
	QString service_create_apache_cmd = "DOTHING_WEB_SERVER";

	QString service_create_mysql_cmd = "DOTHING_DATABASE_SERVER";

	QString service_create_task_cmd = "DOTHING_TASK_SERVER";

	QString service_create_memcache_cmd = "memcached Server";

	QString service_create_nginx_cmd = "nginx";

	QString service_create_wechat_cmd = "WechatServer";

	QStringList cmdList;
	cmdList << service_create_apache_cmd << service_create_mysql_cmd << service_create_task_cmd << service_create_memcache_cmd;
	cmdList << service_create_nginx_cmd << service_create_wechat_cmd;

	foreach(const QString &cmd, cmdList){
		QString execStopCmd = "net stop \"" + cmd + "\"";
		QProcess p(0);
		p.start(execStopCmd);
		p.waitForStarted();
		p.waitForFinished();
		qDebug() << execStopCmd;
		qDebug() << QString::fromLocal8Bit(p.readAllStandardError());
		p.terminate();

		QString execDeleteCmd = "sc delete \"" + cmd + "\"";
		QProcess pDelete(0);
		pDelete.start(execDeleteCmd);
		pDelete.waitForStarted();
		pDelete.waitForFinished();
		qDebug() << execDeleteCmd;
		qDebug() << QString::fromLocal8Bit(pDelete.readAllStandardError());
		pDelete.terminate();
	}

	QProcess taskkill;

	taskkill.execute("taskkill", QStringList() << "-im" << "nginx.exe" << "-f");

	QProcess phpTaskkill;

	phpTaskkill.execute("taskkill", QStringList() << "-im" << "php-cgi.exe" << "-f");

	emit runOverSignal();
}