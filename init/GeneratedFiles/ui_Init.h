/********************************************************************************
** Form generated from reading UI file 'Init.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INIT_H
#define UI_INIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Init
{
public:
    QWidget *centralWidget;

    void setupUi(QMainWindow *Init)
    {
        if (Init->objectName().isEmpty())
            Init->setObjectName(QStringLiteral("Init"));
        Init->resize(400, 300);
        centralWidget = new QWidget(Init);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Init->setCentralWidget(centralWidget);

        retranslateUi(Init);

        QMetaObject::connectSlotsByName(Init);
    } // setupUi

    void retranslateUi(QMainWindow *Init)
    {
        Init->setWindowTitle(QApplication::translate("Init", "Init", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Init: public Ui_Init {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INIT_H
