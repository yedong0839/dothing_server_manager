/********************************************************************************
** Form generated from reading UI file 'Config.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIG_H
#define UI_CONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_Config
{
public:
    QRadioButton *radioButtonPort;
    QRadioButton *radioButtonDocument;
    QLineEdit *lineEditWePort;
    QLineEdit *lineEditWebDocument;
    QPushButton *pushButtonCheckPort;
    QPushButton *pushButtonChooseWebDocument;
    QPushButton *pushButton_3;
    QLabel *label;

    void setupUi(QDialog *Config)
    {
        if (Config->objectName().isEmpty())
            Config->setObjectName(QStringLiteral("Config"));
        Config->resize(360, 163);
        radioButtonPort = new QRadioButton(Config);
        radioButtonPort->setObjectName(QStringLiteral("radioButtonPort"));
        radioButtonPort->setGeometry(QRect(20, 50, 89, 16));
        radioButtonDocument = new QRadioButton(Config);
        radioButtonDocument->setObjectName(QStringLiteral("radioButtonDocument"));
        radioButtonDocument->setGeometry(QRect(20, 80, 89, 16));
        lineEditWePort = new QLineEdit(Config);
        lineEditWePort->setObjectName(QStringLiteral("lineEditWePort"));
        lineEditWePort->setGeometry(QRect(130, 50, 113, 20));
        lineEditWebDocument = new QLineEdit(Config);
        lineEditWebDocument->setObjectName(QStringLiteral("lineEditWebDocument"));
        lineEditWebDocument->setGeometry(QRect(130, 80, 113, 20));
        pushButtonCheckPort = new QPushButton(Config);
        pushButtonCheckPort->setObjectName(QStringLiteral("pushButtonCheckPort"));
        pushButtonCheckPort->setGeometry(QRect(260, 50, 75, 23));
        pushButtonChooseWebDocument = new QPushButton(Config);
        pushButtonChooseWebDocument->setObjectName(QStringLiteral("pushButtonChooseWebDocument"));
        pushButtonChooseWebDocument->setGeometry(QRect(260, 80, 75, 23));
        pushButton_3 = new QPushButton(Config);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(260, 120, 75, 23));
        label = new QLabel(Config);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 321, 16));

        retranslateUi(Config);

        QMetaObject::connectSlotsByName(Config);
    } // setupUi

    void retranslateUi(QDialog *Config)
    {
        Config->setWindowTitle(QApplication::translate("Config", "Dialog", Q_NULLPTR));
        radioButtonPort->setText(QApplication::translate("Config", "WEB \347\253\257\345\217\243\350\256\276\347\275\256", Q_NULLPTR));
        radioButtonDocument->setText(QApplication::translate("Config", "WEB \347\233\256\345\275\225\350\256\276\347\275\256", Q_NULLPTR));
        pushButtonCheckPort->setText(QApplication::translate("Config", "\347\253\257\345\217\243\346\243\200\346\265\213", Q_NULLPTR));
        pushButtonChooseWebDocument->setText(QApplication::translate("Config", "\351\200\211\346\213\251\347\233\256\345\275\225", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("Config", "\345\256\214\346\210\220", Q_NULLPTR));
        label->setText(QApplication::translate("Config", "WEB \346\234\215\345\212\241\345\231\250\350\256\276\347\275\256", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Config: public Ui_Config {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIG_H
