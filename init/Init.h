#ifndef INIT_H
#define INIT_H

#include <QMainWindow>
//#include <QQuickView>

namespace Ui {
class Init;
}

class Init : public QMainWindow
{
    Q_OBJECT

public:
//    explicit Init(QWidget *parent = 0);
    Init();
    ~Init();

private:
    //Ui::Init *ui;
    //void createService();
    QString cur_dir;
    void setMysqlIni();
    void setApacheConf();
    void setPHPIni();
	void setNginxInstallXml();

private slots:
    void startAppSlot();
    void createService();


signals:
    void startAppSignal();
    void createServiceSignal();
    void appExitSignal();
};

#endif // INIT_H
