#include "Init.h"
//#include "ui_Init.h"
#include "Config.h"
#include <QProcess>
#include <QDebug>
#include <QSettings>
#include <qapplication.h>
#include <QFile>

void setApachePhpPre(QString cur_dir, QString apacheRoot) {
	QString old_version_php_conf = "<IfModule env_module>\r\n";
	old_version_php_conf += "SetEnv MIBDIRS \"{dir}/pre/php/extras/mibs\"\r\n";
	old_version_php_conf += "SetEnv MYSQL_HOME \"xampp\\mysql\\bin\"\r\n";
	old_version_php_conf += "SetEnv OPENSSL_CONF \"{dir}/apache/bin/openssl.cnf\"\r\n";
	old_version_php_conf += "SetEnv PHP_PEAR_SYSCONF_DIR \"xampp\\pre\\php\"\r\n";
	old_version_php_conf += "SetEnv PHPRC \"xampp\\pre\\php\"\r\n";
	old_version_php_conf += "SetEnv TMP \"xampp\\tmp\"\r\n";
	old_version_php_conf += " </IfModule>\r\n";

	old_version_php_conf += "LoadFile \"{dir}/pre/php/php5ts.dll\"\r\n";
	old_version_php_conf += "LoadModule php5_module \"{dir}/pre/php/php5apache2_4.dll\"\r\n";

	old_version_php_conf += "<FilesMatch \"\.php$\">\r\n";
	old_version_php_conf += "SetHandler application/x-httpd-php\r\n";
	old_version_php_conf += "</FilesMatch>\r\n";
	old_version_php_conf += "<FilesMatch \"\.phps$\">\r\n";
	old_version_php_conf += "SetHandler application/x-httpd-php-source\r\n";
	old_version_php_conf += "</FilesMatch>\r\n";


	old_version_php_conf += "<IfModule php5_module>\r\n";
	old_version_php_conf += "PHPINIDir \"{dir}/pre/php\"\r\n";
	old_version_php_conf += "</IfModule>\r\n";

	old_version_php_conf += " <IfModule mime_module>\r\n";
	old_version_php_conf += "AddType text/html .php .phps\r\n";
	old_version_php_conf += "</IfModule>\r\n";

	old_version_php_conf += "ScriptAlias /php-cgi/ \"{dir}/pre/php/\"\r\n";
	old_version_php_conf += "<Directory \"{dir}/pre/php\">\r\n";
	old_version_php_conf += "AllowOverride None\r\n";
	old_version_php_conf += "Options None\r\n";
	old_version_php_conf += "Require all denied\r\n";
	old_version_php_conf += "<Files \"php-cgi.exe\">\r\n";
	old_version_php_conf += "Require all granted\r\n";
	old_version_php_conf += "</Files>\r\n";
	old_version_php_conf += "</Directory>\r\n";

	old_version_php_conf += "<Directory \"{dir}/cgi-bin\">\r\n";
	old_version_php_conf += "<FilesMatch \"\.php$\">\r\n";
	old_version_php_conf += "SetHandler cgi-script\r\n";
	old_version_php_conf += "</FilesMatch>\r\n";
	old_version_php_conf += "<FilesMatch \"\.phps$\">\r\n";
	old_version_php_conf += "SetHandler None\r\n";
	old_version_php_conf += "</FilesMatch>\r\n";
	old_version_php_conf += "</Directory>\r\n";

	old_version_php_conf.replace("{dir}", cur_dir + "/server");
	old_version_php_conf.replace("xampp", cur_dir + "/server");
	qDebug() << old_version_php_conf;
	//QString apacheRoot = cur_dir + "/server/apache";
	QFile confFilePre(apacheRoot + "/conf/extra/httpd-dothing-pre.conf");
	qDebug() << old_version_php_conf;
	if (confFilePre.open(QIODevice::WriteOnly)) {
		qDebug() << "i am here";
		confFilePre.write(old_version_php_conf.toLatin1());
		confFilePre.flush();
		confFilePre.close();
	}
	else {
		qDebug() << "打开" + apacheRoot + "/conf/extra/httpd-dothing-pre.conf" + "文件失败";
		return;
	}

}

Init::Init() :
    QMainWindow(NULL)
{
    //QObject *rootObject = reinterpret_cast<QObject*>(viewer->rootObject());

    //connect(updateMgr, SIGNAL(setLoaderText(QVariant)), rootObject, SLOT(updateText(QVariant)));
    connect(this,SIGNAL(startAppSignal()),this,SLOT(startAppSlot()));
    connect(this,SIGNAL(createServiceSignal()),this,SLOT(createService()));
    QObject::connect(this, SIGNAL(appExitSignal()), QApplication::instance(), SLOT(quit()), Qt::QueuedConnection);

#ifdef _DEBUG
    cur_dir = "D:/tools/dothing_server_manager";
#else
    cur_dir = QCoreApplication::applicationDirPath();
#endif

    setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::Dialog);
    setAttribute(Qt::WA_TranslucentBackground);

    qRegisterMetaType<QProcess::ExitStatus>("QProcess::ExitStatus");
    emit createServiceSignal();
}

Init::~Init()
{
    //delete ui;
}

void Init::createService(){

 QString service_create_apache_cmd = QString("%1/server/apache/bin/httpd.exe -k install -n DOTHING_WEB_SERVER").arg(cur_dir);

 QString service_create_mysql_cmd = QString("%1/server/mysql/bin/mysqld --install DOTHING_DATABASE_SERVER --defaults-file=\"%1/server/mysql/bin/my.ini\"").arg(cur_dir).arg(cur_dir);

 QString service_create_task_cmd = QString("%1/server/task/DothingTask.exe -install").arg(cur_dir);

 QString service_create_memcache_cmd = QString("%1/server/memcached.exe -d install").arg(cur_dir);

 QString service_create_nginx_cmd = QString("%1/server/nginx/winsw.exe install").arg(cur_dir);

 QString service_create_wechat_cmd = QString("%1/server/mywechat.exe install").arg(cur_dir);

 //QString service_create_redis_cmd = QString("%1/server/redis/redis-server.exe install").arg(cur_dir);

 QStringList cmdList;
 cmdList << service_create_apache_cmd << service_create_mysql_cmd << service_create_task_cmd << service_create_memcache_cmd << service_create_nginx_cmd << service_create_wechat_cmd;
 //cmdList << service_create_redis_cmd;

 foreach(const QString &cmd,cmdList){
     QProcess p(0);
     p.start(cmd);
     p.waitForStarted();
     p.waitForFinished();
     qDebug()<<cmd;
     qDebug()<<QString::fromLocal8Bit(p.readAllStandardError());
     p.terminate();
 }

    setMysqlIni();
    setApacheConf();
    setPHPIni();
	setNginxInstallXml();
    emit startAppSignal();
}

void Init::startAppSlot(){
    QString file = cur_dir +"/dothing_server_manager.exe";
    QProcess::startDetached(file, QStringList());
    emit appExitSignal();
}

void Init::setMysqlIni(){
    QString serverDir = cur_dir+"/server";
    QSettings settingsD("server/mysql/bin/my.ini", QSettings::IniFormat); // 当前目录的INI文件
    settingsD.beginGroup("mysqld");
    settingsD.setValue("port", 3336);

    settingsD.setValue("socket", serverDir+"/mysql/mysql.sock");
    settingsD.setValue("basedir", serverDir+"/mysql");
    settingsD.setValue("tmpdir", serverDir+"/tmp");
    settingsD.setValue("datadir", serverDir+"/mysql/data");
    settingsD.setValue("plugin_dir", serverDir+"/mysql/lib/plugin/");
    settingsD.setValue("innodb_data_home_dir", serverDir+"/mysql/data");
    settingsD.setValue("innodb_log_group_home_dir", serverDir+"/mysql/data");

    settingsD.endGroup();

    settingsD.beginGroup("client");
    settingsD.setValue("port", 3336);
    settingsD.setValue("socket", serverDir+"/mysql/mysql.sock");
    settingsD.endGroup();
}

void Init::setApacheConf(){
    QString conf = "<IfModule env_module>\r\n";
                conf += "SetEnv MIBDIRS \"{dir}/php/extras/mibs\"\r\n";
                conf += "SetEnv MYSQL_HOME \"xampp\\mysql\\bin\"\r\n";
                conf += "SetEnv OPENSSL_CONF \"{dir}/apache/bin/openssl.cnf\"\r\n";
                conf += "SetEnv PHP_PEAR_SYSCONF_DIR \"xampp\\php\"\r\n";
                conf += "SetEnv PHPRC \"xampp\\php\"\r\n";
                conf += "SetEnv TMP \"xampp\\tmp\"\r\n";
           conf += " </IfModule>\r\n";


            conf += "LoadFile \"{dir}/php/php5ts.dll\"\r\n";
            conf += "LoadModule php5_module \"{dir}/php/php5apache2_4.dll\"\r\n";

            conf += "<FilesMatch \"\.php$\">\r\n";
                conf += "SetHandler application/x-httpd-php\r\n";
            conf += "</FilesMatch>\r\n";
            conf += "<FilesMatch \"\.phps$\">\r\n";
                conf += "SetHandler application/x-httpd-php-source\r\n";
            conf += "</FilesMatch>\r\n";


            conf += "<IfModule php5_module>\r\n";
                conf += "PHPINIDir \"{dir}/php\"\r\n";
            conf += "</IfModule>\r\n";

           conf += " <IfModule mime_module>\r\n";
                conf += "AddType text/html .php .phps\r\n";
            conf += "</IfModule>\r\n";

            conf += "ScriptAlias /php-cgi/ \"{dir}/php/\"\r\n";
            conf += "<Directory \"{dir}/php\">\r\n";
                conf += "AllowOverride None\r\n";
                conf += "Options None\r\n";
                conf += "Require all denied\r\n";
                conf += "<Files \"php-cgi.exe\">\r\n";
                      conf += "Require all granted\r\n";
                conf += "</Files>\r\n";
            conf += "</Directory>\r\n";

            conf += "<Directory \"{dir}/cgi-bin\">\r\n";
                conf += "<FilesMatch \"\.php$\">\r\n";
                    conf += "SetHandler cgi-script\r\n";
                conf += "</FilesMatch>\r\n";
                conf += "<FilesMatch \"\.phps$\">\r\n";
                    conf += "SetHandler None\r\n";
                conf += "</FilesMatch>\r\n";
            conf += "</Directory>\r\n";

            conf.replace("{dir}",cur_dir+"/server");
            conf.replace("xampp",cur_dir+"/server");

            QString apacheRoot = cur_dir+"/server/apache";
            QFile confFile(apacheRoot+"/conf/extra/httpd-dothing.conf");

            if(confFile.open(QIODevice::WriteOnly)){
                confFile.write(conf.toLatin1());
                confFile.flush();
                confFile.close();
            }else{
                qDebug()<<"打开"+apacheRoot+"/conf/extra/httpd-dothing.conf"+"文件失败";
                return;
            }

			/*配置apache与PHP的关联文件*/
			
		
			/*php5.4*/
			QString old_version_php54_conf = "<IfModule env_module>\r\n";
			old_version_php54_conf += "SetEnv MIBDIRS \"{dir}/pre/php54/extras/mibs\"\r\n";
			old_version_php54_conf += "SetEnv MYSQL_HOME \"xampp\\mysql\\bin\"\r\n";
			old_version_php54_conf += "SetEnv OPENSSL_CONF \"{dir}/apache/bin/openssl.cnf\"\r\n";
			old_version_php54_conf += "SetEnv PHP_PEAR_SYSCONF_DIR \"xampp\\pre\\php54\"\r\n";
			old_version_php54_conf += "SetEnv PHPRC \"xampp\\pre\\php54\"\r\n";
			old_version_php54_conf += "SetEnv TMP \"xampp\\tmp\"\r\n";
			old_version_php54_conf += " </IfModule>\r\n";

			old_version_php54_conf += "LoadFile \"{dir}/pre/php54/php5ts.dll\"\r\n";
			old_version_php54_conf += "LoadModule php5_module \"{dir}/pre/php54/php5apache2_4.dll\"\r\n";

			old_version_php54_conf += "<FilesMatch \"\.php$\">\r\n";
			old_version_php54_conf += "SetHandler application/x-httpd-php\r\n";
			old_version_php54_conf += "</FilesMatch>\r\n";
			old_version_php54_conf += "<FilesMatch \"\.phps$\">\r\n";
			old_version_php54_conf += "SetHandler application/x-httpd-php-source\r\n";
			old_version_php54_conf += "</FilesMatch>\r\n";


			old_version_php54_conf += "<IfModule php5_module>\r\n";
			old_version_php54_conf += "PHPINIDir \"{dir}/pre/php54\"\r\n";
			old_version_php54_conf += "</IfModule>\r\n";

			old_version_php54_conf += " <IfModule mime_module>\r\n";
			old_version_php54_conf += "AddType text/html .php .phps\r\n";
			old_version_php54_conf += "</IfModule>\r\n";

			old_version_php54_conf += "ScriptAlias /php-cgi/ \"{dir}/pre/php54/\"\r\n";
			old_version_php54_conf += "<Directory \"{dir}/pre/php54\">\r\n";
			old_version_php54_conf += "AllowOverride None\r\n";
			old_version_php54_conf += "Options None\r\n";
			old_version_php54_conf += "Require all denied\r\n";
			old_version_php54_conf += "<Files \"php-cgi.exe\">\r\n";
			old_version_php54_conf += "Require all granted\r\n";
			old_version_php54_conf += "</Files>\r\n";
			old_version_php54_conf += "</Directory>\r\n";

			old_version_php54_conf += "<Directory \"{dir}/cgi-bin\">\r\n";
			old_version_php54_conf += "<FilesMatch \"\.php$\">\r\n";
			old_version_php54_conf += "SetHandler cgi-script\r\n";
			old_version_php54_conf += "</FilesMatch>\r\n";
			old_version_php54_conf += "<FilesMatch \"\.phps$\">\r\n";
			old_version_php54_conf += "SetHandler None\r\n";
			old_version_php54_conf += "</FilesMatch>\r\n";
			old_version_php54_conf += "</Directory>\r\n";

			old_version_php54_conf.replace("{dir}", cur_dir + "/server");
			old_version_php54_conf.replace("xampp", cur_dir + "/server");

			//QString apacheRoot = cur_dir + "/server/apache";
			QFile confFilePre54(apacheRoot + "/conf/extra/httpd-dothing-pre54.conf");

			if (confFilePre54.open(QIODevice::WriteOnly)){
				confFilePre54.write(old_version_php54_conf.toLatin1());
				confFilePre54.flush();
				confFilePre54.close();
			}
			else{
				qDebug() << "打开" + apacheRoot + "/conf/extra/httpd-dothing-pre54.conf" + "文件失败";
				return;
			}

			/*php7.1*/
			QString php71_conf = "<IfModule env_module>\r\n";
			php71_conf += "SetEnv MIBDIRS \"{dir}/pre/php71/extras/mibs\"\r\n";
			php71_conf += "SetEnv MYSQL_HOME \"xampp\\mysql\\bin\"\r\n";
			php71_conf += "SetEnv OPENSSL_CONF \"{dir}/apache/bin/openssl.cnf\"\r\n";
			php71_conf += "SetEnv PHP_PEAR_SYSCONF_DIR \"xampp\\pre\\php71\"\r\n";
			php71_conf += "SetEnv PHPRC \"xampp\\pre\\php71\"\r\n";
			php71_conf += "SetEnv TMP \"xampp\\tmp\"\r\n";
			php71_conf += " </IfModule>\r\n";

			php71_conf += "LoadFile \"{dir}/pre/php71/php7ts.dll\"\r\n";
			php71_conf += "LoadModule php7_module \"{dir}/pre/php71/php7apache2_4.dll\"\r\n";

			php71_conf += "<FilesMatch \"\.php$\">\r\n";
			php71_conf += "SetHandler application/x-httpd-php\r\n";
			php71_conf += "</FilesMatch>\r\n";
			php71_conf += "<FilesMatch \"\.phps$\">\r\n";
			php71_conf += "SetHandler application/x-httpd-php-source\r\n";
			php71_conf += "</FilesMatch>\r\n";


			php71_conf += "<IfModule php7_module>\r\n";
			php71_conf += "PHPINIDir \"{dir}/pre/php71\"\r\n";
			php71_conf += "</IfModule>\r\n";

			php71_conf += " <IfModule mime_module>\r\n";
			php71_conf += "AddType text/html .php .phps\r\n";
			php71_conf += "</IfModule>\r\n";

			php71_conf += "ScriptAlias /php-cgi/ \"{dir}/pre/php71/\"\r\n";
			php71_conf += "<Directory \"{dir}/pre/php71\">\r\n";
			php71_conf += "AllowOverride None\r\n";
			php71_conf += "Options None\r\n";
			php71_conf += "Require all denied\r\n";
			php71_conf += "<Files \"php-cgi.exe\">\r\n";
			php71_conf += "Require all granted\r\n";
			php71_conf += "</Files>\r\n";
			php71_conf += "</Directory>\r\n";

			php71_conf += "<Directory \"{dir}/cgi-bin\">\r\n";
			php71_conf += "<FilesMatch \"\.php$\">\r\n";
			php71_conf += "SetHandler cgi-script\r\n";
			php71_conf += "</FilesMatch>\r\n";
			php71_conf += "<FilesMatch \"\.phps$\">\r\n";
			php71_conf += "SetHandler None\r\n";
			php71_conf += "</FilesMatch>\r\n";
			php71_conf += "</Directory>\r\n";


			php71_conf.replace("{dir}", cur_dir + "/server");
			php71_conf.replace("xampp", cur_dir + "/server");

			//QString apacheRoot = cur_dir + "/server/apache";
			QFile confFilePre71(apacheRoot + "/conf/extra/httpd-dothing-pre71.conf");

			if (confFilePre71.open(QIODevice::WriteOnly)) {
				confFilePre71.write(php71_conf.toLatin1());
				confFilePre71.flush();
				confFilePre71.close();
			}
			else {
				qDebug() << "打开" + apacheRoot + "/conf/extra/httpd-dothing-pre71.conf" + "文件失败";
				return;
			}
			//设置pre php
			setApachePhpPre(cur_dir, apacheRoot);
			/*配置apache与PHP的关联文件 结束*/

			/*配置SSL*/
            QString confSSL = "Listen 443\r\n";

                    confSSL += "SSLCipherSuite HIGH:MEDIUM:!aNULL:!MD5\r\n";

                    confSSL += "SSLPassPhraseDialog  builtin\r\n";

                    confSSL += "SSLSessionCache \"shmcb:{dir}/apache/logs/ssl_scache(512000)\"\r\n";
                    confSSL += "SSLSessionCacheTimeout  300\r\n";


                    confSSL += "<VirtualHost _default_:443>\r\n";

                    confSSL += "ServerName www.example.com:443\r\n";
                    confSSL += "ServerAdmin admin@example.com\r\n";
                    confSSL += "ErrorLog \"{dir}/apache/logs/error.log\"\r\n";
                    confSSL += "TransferLog \"{dir}/apache/logs/access.log\"\r\n";

                    confSSL += "SSLEngine on\r\n";


                    confSSL += "SSLCertificateFile \"conf/ssl.crt/server.crt\"\r\n";

                    confSSL += "SSLCertificateKeyFile \"conf/ssl.key/server.key\"\r\n";


                    confSSL += "<FilesMatch \"\.(cgi|shtml|phtml|php)$\">\r\n";
                    confSSL += "    SSLOptions +StdEnvVars\r\n";
                    confSSL += "</FilesMatch>\r\n";
                    confSSL += "<Directory \"{dir}/apache/cgi-bin\">\r\n";
                    confSSL += "    SSLOptions +StdEnvVars\r\n";
                    confSSL += "</Directory>\r\n";

                    confSSL += "BrowserMatch \"MSIE [2-5]\" \\\r\n";
                    confSSL += "         nokeepalive ssl-unclean-shutdown \\\r\n";
                    confSSL += "         downgrade-1.0 force-response-1.0\r\n";
                    confSSL += "CustomLog \"{dir}/apache/logs/ssl_request.log\" \\\r\n";
                    confSSL += "          \"%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \\\"%r\\\" %b\"\r\n";

                   confSSL += " </VirtualHost>\r\n";
                    //confSSL += "DocumentRoot \"{webroot}/www\"\r\n";

            confSSL.replace("{dir}",cur_dir+"/server");
            confSSL.replace("{webroot}",cur_dir);

            QFile confSSLFile(apacheRoot+"/conf/extra/httpd-ssl.conf");
            if(confSSLFile.open(QIODevice::WriteOnly)){
                confSSLFile.write(confSSL.toLatin1());
                confSSLFile.flush();
                confSSLFile.close();
            }else{
                qDebug()<<"打开"+apacheRoot+"/conf/extra/httpd-ssl.conf"+"文件失败";
                return;
            }
			/*配置SSL 结束*/

			/*配置使用哪一个PHP版本*/
			QFile paramTpl(apacheRoot+"/conf/extra/param-tpl.conf");
			if (paramTpl.open(QIODevice::ReadOnly)){

				QByteArray data = paramTpl.readAll();
				QString dataString = data;
				dataString.replace("{dir}", cur_dir);
				paramTpl.flush();
				paramTpl.close();

				QFile param(apacheRoot+ "/conf/extra/param.conf");
				param.open(QIODevice::WriteOnly);
				param.write(dataString.toLatin1());
				param.flush();
				param.close();

			}
			else{
				qDebug() << "打开" + apacheRoot+ "/conf/extra/param.conf" + "文件失败";
				return;
			}

			/*设置默认的virtualhost*/
			QFile virtualHostTpl (apacheRoot + "/conf/extra/httpd-vhosts-tpl.conf");
			virtualHostTpl.open(QIODevice::ReadOnly);

			QByteArray dataVirtual = virtualHostTpl.readAll();
			QString dataStringVirtual = dataVirtual;
			dataStringVirtual.replace("{dir}", cur_dir);
			virtualHostTpl.flush();
			virtualHostTpl.close();

			QFile virtualHost(apacheRoot + "/conf/extra/httpd-vhosts.conf");
			virtualHost.open(QIODevice::WriteOnly);
			virtualHost.write(dataStringVirtual.toLatin1());
			virtualHost.flush();
			virtualHost.close();

			QSettings settingsVirtualHost("virtualhost.ini", QSettings::IniFormat); // 当前目录的INI文件
			settingsVirtualHost.setValue("num/web", 1);
			settingsVirtualHost.setValue("num1/port", 8080);
			settingsVirtualHost.setValue("num1/documentRoot", cur_dir + "/www");
			settingsVirtualHost.setValue("num1/phpIncludePath", cur_dir + "/www");

            QSettings settings("web.ini", QSettings::IniFormat); // 当前目录的INI文件
            settings.beginGroup("web");
            settings.setValue("documentRoot", cur_dir+"/www");
            settings.setValue("port", 8080);
			settings.setValue("defaultphp", 53);
            settings.endGroup();

}

/*
	设置各版本php ini
	在相应版本PHP的根目录里param里放置了一份ini模板
*/
void Init::setPHPIni(){

	/*php5.5*/
    QFile phpPramIni(cur_dir+"/server/php/param/php.ini");
    if(phpPramIni.open(QIODevice::ReadOnly)){

        QByteArray data = phpPramIni.readAll();
        QString dataString = data;
        dataString.replace("{dir}",cur_dir);
        phpPramIni.flush();
        phpPramIni.close();

        QFile phpIni(cur_dir+"/server/php/php.ini");
        phpIni.open(QIODevice::WriteOnly);
        phpIni.write(dataString.toLatin1());
        phpIni.flush();
        phpIni.close();

    }else{
        qDebug()<<"打开"+cur_dir+"/server/php/php.ini"+"文件失败";
    }

	/*php5.2*/
	QFile phpPramIniPre(cur_dir + "/server/pre/php/param/php.ini");
	qDebug() << "打开" + cur_dir + "/server/pre/php/param/php.ini" + "准备";
	if (phpPramIniPre.open(QIODevice::ReadOnly)){
		qDebug() << "打开" + cur_dir + "/server/pre/php/param/php.ini" + "成功";
		QByteArray dataPre = phpPramIniPre.readAll();
		QString dataStringPre = dataPre;
		dataStringPre.replace("{dir}", cur_dir);
		phpPramIniPre.flush();
		phpPramIniPre.close();

		QFile phpIniPre(cur_dir + "/server/pre/php/php.ini");

		if (phpIniPre.open(QIODevice::WriteOnly)){
			qDebug() << "打开" + cur_dir + "/server/pre/php/php.ini" + "成功";
			//qDebug() << dataStringPre;
			phpIniPre.write(dataStringPre.toLatin1());
			phpIniPre.flush();
			phpIniPre.close();
		}
		else{
			qDebug() << "打开" + cur_dir + "/server/pre/php/php.ini" + "失败";
		}

	}
	else{
		qDebug() << "打开" + cur_dir + "/server/pre/php/php.ini" + "文件失败";
	}

	/*php5.4*/
	QFile phpPramIniPre54(cur_dir + "/server/pre/php54/param/php.ini");
	qDebug() << "打开" + cur_dir + "/server/pre/php54/param/php.ini" + "准备";
	if (phpPramIniPre54.open(QIODevice::ReadOnly)){
		qDebug() << "打开" + cur_dir + "/server/pre/php54/param/php.ini" + "成功";
		QByteArray dataPre54 = phpPramIniPre54.readAll();
		QString dataStringPre54 = dataPre54;
		dataStringPre54.replace("{dir}", cur_dir);
		phpPramIniPre54.flush();
		phpPramIniPre54.close();


		QFile phpIniPre54(cur_dir + "/server/pre/php54/php.ini");

		if (phpIniPre54.open(QIODevice::WriteOnly)){
			qDebug() << "打开" + cur_dir + "/server/pre/php54/php.ini" + "成功";
			//qDebug() << dataStringPre;
			phpIniPre54.write(dataStringPre54.toLatin1());
			phpIniPre54.flush();
			phpIniPre54.close();
		}
		else{
			qDebug() << "打开" + cur_dir + "/server/pre/php54/php.ini" + "失败";
		}
	}
	else{
		qDebug() << "打开" + cur_dir + "/server/pre/php/php.ini" + "文件失败";
	}

	/*php7.1*/
	QFile phpPramIniPre71(cur_dir + "/server/pre/php71/param/php.ini");
	qDebug() << "打开" + cur_dir + "/server/pre/php71/param/php.ini" + "准备";
	if (phpPramIniPre71.open(QIODevice::ReadOnly)) {
		qDebug() << "打开" + cur_dir + "/server/pre/php71/param/php.ini" + "成功";
		QByteArray dataPre71 = phpPramIniPre71.readAll();
		QString dataStringPre71 = dataPre71;
		dataStringPre71.replace("{dir}", cur_dir);
		phpPramIniPre71.flush();
		phpPramIniPre71.close();


		QFile phpIniPre71(cur_dir + "/server/pre/php71/php.ini");

		if (phpIniPre71.open(QIODevice::WriteOnly)) {
			qDebug() << "打开" + cur_dir + "/server/pre/php54/php.ini" + "成功";
			//qDebug() << dataStringPre;
			phpIniPre71.write(dataStringPre71.toLatin1());
			phpIniPre71.flush();
			phpIniPre71.close();
		}
		else {
			qDebug() << "打开" + cur_dir + "/server/pre/php71/php.ini" + "失败";
		}
	}
	else {
		qDebug() << "打开" + cur_dir + "/server/pre/php71/param/php.ini" + "文件失败";
	}

}

void Init::setNginxInstallXml()
{
	QFile installXml(cur_dir + "/server/nginx/winsw.tmpl");
	QString nginx = cur_dir + "/server/nginx/nginx.exe";
	if (!installXml.open(QIODevice::ReadOnly)){
		qDebug() << "open winsw xml file fail";
		return;
	}
	QByteArray contentByte = installXml.readAll();
	QString content = contentByte;
	content.replace("$${exe}", nginx);
	content.replace("$${logs}", cur_dir + "/server/");
	installXml.flush();
	installXml.close();

	QFile confTarget(cur_dir + "/server/nginx/winsw.xml");

	if (!confTarget.open(QIODevice::WriteOnly)){
		qDebug() << "open nginx conf file fail";
		return;
	}

	confTarget.write(content.toLatin1());
	confTarget.flush();
	confTarget.close();

}


