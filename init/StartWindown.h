#ifndef STARTWINDOWN_H
#define STARTWINDOWN_H

#include <QQuickPaintedItem>

class StartWindown : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor)
public:
    StartWindown(QQuickItem *parent = 0);

    void paint(QPainter *painter);

    QColor color() const;
    void setColor(const QColor &color);
private:
     QColor m_color;
signals:

public slots:

};

#endif // STARTWINDOWN_H
