#include "StartWindown.h"
#include <QPen>
#include <QPainter>
#include <QColor>

StartWindown::StartWindown(QQuickItem *parent) :
    QQuickPaintedItem(parent)
{
    //setFlags (Qt::FramelessWindowHint);
    //setFlag(Qt::FramelessWindowHint);
    //setWindowFlags(Qt::FramelessWindowHint |Qt::WindowStaysOnTopHint);
}
void StartWindown::paint(QPainter *painter)
{
    QPen pen(QColor("red"),2);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing, true);
    painter->drawRect(0,0,400,400);
}
QColor StartWindown::color() const
{
    return m_color;
}

void StartWindown::setColor(const QColor &color)
{
    m_color = color;
}
