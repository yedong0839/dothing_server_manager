#include "Config.h"
#include "ui_Config.h"
#include <QFileDialog>

Config::Config(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Config)
{
    QString cur_dir = QCoreApplication::applicationDirPath();

    QString webDocument = cur_dir+"/www";
    ui->lineEditWebDocument->setText(webDocument);
    ui->setupUi(this);
}

Config::~Config()
{
    delete ui;
}

void Config::on_pushButtonChooseWebDocument_pressed()
{
    QString flag = QStringLiteral("Web");
    QString dir = QFileDialog::getExistingDirectory(this,flag,
                                                    "/",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    if(dir != ""){
        ui->lineEditWebDocument->setText(dir);
    }else{

    }
}
