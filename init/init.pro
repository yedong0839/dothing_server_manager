#-------------------------------------------------
#
# Project created by QtCreator 2015-07-29T11:55:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets qml quick

TARGET = init
TEMPLATE = app


SOURCES += main.cpp\
        Init.cpp \
    StartWindown.cpp \
    Config.cpp

HEADERS  += Init.h \
    StartWindown.h \
    Config.h

FORMS    += Init.ui \
    Config.ui

RESOURCES += \
    init.qrc
