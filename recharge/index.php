<?php

require 'Slim/Slim.php';
require 'config.php';

\Slim\Slim::registerAutoloader();
use \Slim\Curl;

$app = new \Slim\Slim();
$app->view(new \Slim\JsonApiView());
$app->add(new \Slim\JsonApiMiddleware());

$authenticate = function ( ) {
    return function () {
        if (!\Slim\Sign::checkSign()) {//验证签名
            $app = \Slim\Slim::getInstance();
            $app->flash('error', 'authenticate fail');
            $app->redirect('checkSignFail');
        }
    };
};


/**
 * 动态sessionkey获取接口
 */
$app->get("/getSessionKey", $authenticate(),function () use ($app) {

    $arr_param = array(
        'app'=>APP,
        'method'=>'getsessionkey',
        'time'=>time(),
        'format'=>FORMAT
    );

    $str_sign = 'app='.$arr_param['app'].'&method='.$arr_param['method'].'&time='.$arr_param['time']
        .'&key='.KEY;
    $hash = md5($str_sign);

    $arr_param['hash'] = $hash;

    curlFunc($arr_param);

});
/**
 * 计费页面在合作方侧接口 applyForPurchase
 */
$app->get("/applyForPurchase/:tel&:consumecode&:salechannelid&:sessionkey",
                    $authenticate(),
                    function ($tel,$consumecode,$salechannelid,$sessionkey){

                $arr_param = array(
                    'app'=>APP,
                    'method'=>'applyforpurchase',
                    'tel'=>$tel,
                    'consumecode'=>$consumecode,
                    'salechannelid'=>$salechannelid,
                    'time'=>time(),
                    'sessionkey'=>$sessionkey,
                    'format'=>FORMAT
                );

            $str_sign = 'app='.$arr_param['app'].'&method='.$arr_param['method'].'&tel='.$arr_param['tel']
                                 .'&consumecode='.$arr_param['consumecode'].'&time='.$arr_param['time'].'&sessionkey='.$arr_param['sessionkey']
                                 .'&key='.KEY;
             $hash = md5($str_sign);

             $arr_param['hash'] = $hash;

             curlFunc($arr_param);

});
/**
 * 计费页面在合作方侧接口 applyForSubscribe
 */
$app->get("/applyForSubscribe/:tel&:consumecode&:salechannelid&:sessionkey",
    $authenticate(),
    function ($tel,$consumecode,$salechannelid,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'applyforsubscribe',
            'tel'=>$tel,
            'consumecode'=>$consumecode,
            'salechannelid'=>$salechannelid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT
        );

        $str_sign = 'app='.$arr_param['app'].'&method='.$arr_param['method'].'&tel='.$arr_param['tel']
            .'&consumecode='.$arr_param['consumecode'].'&time='.$arr_param['time'].'&sessionkey='.$arr_param['sessionkey']
            .'&key='.KEY;
        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

/**
 * 计费页面在合作方侧接口 applyforunsubscribe
 */
$app->get("/applyForUnsubscribe/:tel&:consumecode&:salechannelid&:sessionkey",
    $authenticate(),
    function ($tel,$consumecode,$salechannelid,$sessionkey)  {

        $arr_param = array(
            'app'=>APP,
            'method'=>'applyforunsubscribe',
            'tel'=>$tel,
            'consumecode'=>$consumecode,
            'salechannelid'=>$salechannelid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT
        );

        $str_sign = 'app='.$arr_param['app'].'&method='.$arr_param['method'].'&tel='.$arr_param['tel']
            .'&consumecode='.$arr_param['consumecode'].'&time='.$arr_param['time'].'&sessionkey='.$arr_param['sessionkey']
            .'&key='.KEY;

        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);
    });

/**
 * 请求确认接口confirmPurchase
 */
$app->get("/confirmPurchase/:verifycode&:orderid&:sessionkey",
    $authenticate(),
    function ($verifycode,$orderid,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'confirmpurchase',
            'verifycode'=>$verifycode,
            'orderid'=>$orderid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT
        );

        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&verifycode='.$arr_param['verifycode']
            .'&orderid='.$arr_param['orderid'].'&time='.$arr_param['time'].'&sessionkey='.$arr_param['sessionkey']
            .'&key='.KEY;
        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

/**
 * 请求确认接口 confirmsubscribe
 */
$app->get("/confirmSubscribe/:verifycode&:orderid&:sessionkey",
    $authenticate(),
    function ($verifycode,$orderid,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'confirmsubscribe',
            'verifycode'=>$verifycode,
            'orderid'=>$orderid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT
        );

        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&verifycode='.$arr_param['verifycode']
            .'&orderid='.$arr_param['orderid'].'&time='.$arr_param['time'].'&sessionkey='.$arr_param['sessionkey']
            .'&key='.KEY;
        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

/**
 * 请求确认接口 confirmunsubscribe
 */
$app->get("/confirmUnsubscribe/:verifycode&:orderid&:sessionkey",
    $authenticate(),
    function ($verifycode,$orderid,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'confirmunsubscribe',
            'verifycode'=>$verifycode,
            'orderid'=>$orderid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT
        );

        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&verifycode='.$arr_param['verifycode']
            .'&orderid='.$arr_param['orderid'].'&time='.$arr_param['time'].'&sessionkey='.$arr_param['sessionkey']
            .'&key='.KEY;
        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

/**
 * 计费回调接口
 */
$app->get("/informPurchaseResult/:result&:orderid&:sessionkey",
    $authenticate(),
    function ($result,$orderid,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'informpurchaseresult',
            'result'=>$result,
            'orderid'=>$orderid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT
        );

        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&result='.$arr_param['result']
            .'&orderid='.$arr_param['orderid'].'&time='.$arr_param['time'] .'&key='.KEY;
        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

/**
 * 单笔对账接口
 */
$app->get("/queryPurchase/:orderid&:sessionkey",
    $authenticate(),
    function ($orderid,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'querypurchase',
            'orderid'=>$orderid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT
        );

        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&orderid='.$arr_param['orderid']
                        .'&time='.$arr_param['time'] .'&sessionkey='.$arr_param['sessionkey'].'&key='.KEY;

        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

/**
 * 查询包月订购状态接口
 */
$app->get("/subscribeQuery/:orderid&:verifycode&:sessionkey",
    $authenticate(),
    function ($orderid,$verifycode,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'subscribequery',
            'orderid'=>$orderid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT,
//            'verifycode'=>$verifycode,
        );

//        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&verifycode='.$verifycode
//            .'&orderid='.$arr_param['orderid'].'&time='.$arr_param['time'].'&sessionkey='.$sessionkey
//            .'&key='.KEY;
       $str_sign = 'app='.APP.'&method='.$arr_param['method']
                        .'&orderid='.$arr_param['orderid'].'&time='.$arr_param['time'].'&sessionkey='.$sessionkey
                        .'&key='.KEY;
        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

/**
 * 计费页面在游戏基地侧接口
 * param:
 */
$app->get("/getUrl/:action&:uid&:salechannelid&:serverid&:notice&:parameter&:sessionkey",
    $authenticate(),
    function ($action,$uid,$salechannelid,$serverid,$notice,$parameter,$sessionkey) {

        $arr_param = array(
            'app'=>APP,
            'method'=>'geturl',
            'action'=>$action,
            'uid'=>$uid,
            'salechannelid'=>$salechannelid,
            'serverid'=>$serverid,
            'time'=>time(),
            'sessionkey'=>$sessionkey,
            'format'=>FORMAT,
            'notice'=>$notice,
            'parameter'=>$parameter
        );

//        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&verifycode='.$verifycode
//            .'&orderid='.$arr_param['orderid'].'&time='.$arr_param['time'].'&sessionkey='.$sessionkey
//            .'&key='.KEY;
        $str_sign = 'app='.APP.'&method='.$arr_param['method'].'&action='.$arr_param['action'].'&uid='.$arr_param['uid']
            .'&salechannelid='.$arr_param['salechannelid'].'&serverid='.$arr_param['serverid'].'&time='.$arr_param['time']
            .'&sessionkey='.$sessionkey
            .'&key='.KEY;
        $hash = md5($str_sign);

        $arr_param['hash'] = $hash;

        curlFunc($arr_param);

    });

$app->get('/checkSignFail', function () use ($app) {
    $app->render(401,array(
        'error' => TRUE,
        'msg'   => 'authenticate fail',
    ));
});

function curlFunc($arr_param){

    $curl = Curl::getInstance();

    $curl->success(function($instance){
        $app = \Slim\Slim::getInstance();
        $app->render(200,array(
            'error' => false,
            'data'=>$instance->response
        ));
    });

    $curl->error(function($instance){
        $app = \Slim\Slim::getInstance();
        $app->render(200,array(
            'error' => true,
            'msg'=>$instance->errorMessage,
            'errorcode'=>$instance->errorCode,
        ));
    });

    $curl->get(URL_BASE,$arr_param);
}

$app->run();

