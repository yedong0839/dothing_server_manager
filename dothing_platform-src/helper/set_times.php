<?php
/**
 * 生成许可证号
 *
 * Example:
 * generate_license('360', '2010-1-1')
 *
 * @param integer $day 注册天数
 * @param string $date 注册日期
 * @return string
 */
function generate_license($day, $date)
{
	$date = explode('-', $date);
	$mktime = mktime(0, 0, 0, $date[1], $date[2], $date[0]);
	return dothing_encode($day . '#' . $mktime);
}

/**
 * 加密 PHP 文件(必须是纯 PHP 代码)
 *
 * Example:
 * dothing_file_encode('./example.php', './decode')
 *
 * @param string $file 待加密文件
 * @param string $out_path 输出路径
 * @return boolean
 */
function dothing_file_encode($file, $out_path)
{
	if (substr($file, strrpos($file, '.')) !== '.php') {
	   return false;
	}

	$source = php_strip_whitespace($file);
	if (null == $source) {
	   return false;
	}

	if (substr($source, 0, 5) == '<?php') {
		$source = ltrim($source, '<?php');
	}
	if (substr($source, 0, 2) == '<?') {
		$source = ltrim($source, '<?');
	}
	$source = rtrim($source, '?>');
	$source = trim($source);

	$out_path = rtrim($out_path, '/\\');

	$out  = $out_path . DIRECTORY_SEPARATOR . basename($file);
	$name = basename($file, '.php');
	$fp = fopen($out, 'w');
	if ($fp) {
		$encode  = "<?php\n";
		$encode .= 'dothing_eval("' . dothing_encode($source) . '");';
		if ($encode) {
            fputs($fp, $encode);
		}
	}
	fclose($fp);

	return true;
}