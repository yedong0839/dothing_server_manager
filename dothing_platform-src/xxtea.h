#ifndef DOTHING_PLATFORM_XXTEA_H
#define DOTHING_PLATFORM_XXTEA_H

#if defined(_MSC_VER)

typedef unsigned __int32 u32;

#else

#if defined(__FreeBSD__) && __FreeBSD__ < 5
#include <inttypes.h>
#else
#include <stdint.h>
#endif

typedef uint32_t u32;

#endif

#define XXTEA_MX (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z)
#define XXTEA_DELTA 0x9e3779b9

void xxtea_encrypt(u32 *v, u32 len, u32 *k);
void xxtea_decrypt(u32 *v, u32 len, u32 *k);

#endif