#ifndef PHP_DOTHING_PLATFORM_H
#define PHP_DOTHING_PLATFORM_H

extern zend_module_entry dothing_platform_module_entry;
#define phpext_dothing_platform_ptr &dothing_platform_module_entry

#ifdef PHP_WIN32
#define PHP_DOTHING_PLATFORM_API __declspec(dllexport)
#else
#define PHP_DOTHING_PLATFORM_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

#define E_THROW(msg, code) zend_throw_exception(NULL, (msg), (code) TSRMLS_CC)

PHP_MINIT_FUNCTION(dothing_platform);
PHP_MSHUTDOWN_FUNCTION(dothing_platform);
PHP_RINIT_FUNCTION(dothing_platform);
PHP_RSHUTDOWN_FUNCTION(dothing_platform);
PHP_MINFO_FUNCTION(dothing_platform);

PHP_FUNCTION(dothing_encode);
PHP_FUNCTION(dothing_encode_2010_scmyth);
//PHP_FUNCTION(dothing_decode);
PHP_FUNCTION(dothing_eval);
//PHP_FUNCTION(dothing_init);

PHP_FUNCTION(d_auth);
PHP_FUNCTION(d_conn);

PHP_FUNCTION(d_run_index);
PHP_FUNCTION(d_run_browse);
PHP_FUNCTION(d_run_act_submit);
PHP_FUNCTION(d_run_index_submit);


PHP_FUNCTION(d_get_ob);


ZEND_BEGIN_MODULE_GLOBALS(dothing_platform)
	unsigned char *license;
ZEND_END_MODULE_GLOBALS(dothing_platform)

#ifdef ZTS
#define DOTHING_PLATFORM_G(v) TSRMG(dothing_platform_globals_id, zend_dothing_platform_globals *, v)
#else
#define DOTHING_PLATFORM_G(v) (dothing_platform_globals.v)
#endif

#endif