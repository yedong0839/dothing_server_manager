import logging
import sys
logger = logging.getLogger('report_tool')

data = {}
__s_game_id = ""
__s_channel_id = ""
__s_game_version = ""

def set_game_name(game_id, channel_id, game_version):
    data['__s_game_id'] = game_id
    data['__s_channel_id'] = channel_id
    data['__s_game_version'] = game_version

def send_err_to_client(err_id = 11):
    return

def send_process_data(pct):
    str =  'pct||%s||%s||%s**%d' % (data['__s_game_id'], data['__s_channel_id'], data['__s_game_version'], pct)
    print str
    sys.stdout.flush()


def send_err_to_srv(cmd, stdoutput, erroutput):
    logger.info(cmd)
    logger.info(stdoutput)
    logger.info(erroutput)

    sys.stdout.flush()
    # packageName = ''
    # idChannel = int(threading.currentThread().getName())
    # channel = ConfigParse.shareInstance().findChannel(idChannel)
    # if channel != None and channel.get('packNameSuffix') != None:
    #     packageName = str(channel['packNameSuffix'])
    #     channelName = str(channel['name'])
    #     if platform.system() == 'Windows':
    #         channelName = str(channel['name']).encode('gbk')
    #     else:
    #         channelName = channel['name'].decode('utf8').encode('gbk')
    # errorOuput = '==================>>>> ERROR <<<<==================\r\n'
    # errorOuput += '[AnySDK_Channel]: ' + threading.currentThread().getName() + '\r\n'
    # errorOuput += '[AnySDK_ChannelName]: ' + channelName + '\r\n'
    # errorOuput += '[AnySDK_Package]: ' + packageName + '\r\n'
    # errorOuput += '[AnySDK_Time]: ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())) + '\r\n'
    # errorOuput += '[AnySDK_Error]:\r\n'
    # errorOuput += stdoutput + '\r\n'
    # errorOuput += erroutput + '\r\n'
    # errorOuput += '=================================================\r\n'
    # log(errorOuput)