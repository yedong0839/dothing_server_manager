; -- UninstallCodeExample1.iss --
;
; This script shows various things you can achieve using a [Code] section for Uninstall

[Setup]
AppName=快发大师打包工具
AppVersion=1.0
DefaultDirName={pf}\KuaiFa
DefaultGroupName=快发大师打包工具
;UninstallDisplayIcon={app}\MyProg.exe
OutputDir=userdocs:Inno Setup Examples Output

[Files]
;Source: "MyProg.exe"; DestDir: "{app}"
;Source: "MyProg.chm"; DestDir: "{app}"
Source: "D:\auto_package\qt_client\vs_proj\auto_package\release_deps\AppLoader.exe"; DestDir: "{app}"Source: "D:\auto_package\qt_client\vs_proj\auto_package\release_deps\*"; DestDir: "{app}"; Flags: recursesubdirs
Source: "D:\auto_package\qt_client\vs_proj\auto_package\release_deps\Readme.txt"; DestDir: "{app}"; Flags: isreadmeSource: "D:\auto_package\qt_client\vs_proj\auto_package\release_deps\auto_package.ico"; DestDir: "{app}"
[Languages]
Name: "zh"; MessagesFile: "compiler:Languages\Chinese.isl"

[Icons]
Name:"{commondesktop}\快发大师";Filename:"{app}\AppLoader.exe";Tasks:desktopicon;WorkingDir: "{app}";IconFilename:{app}\auto_package.ico;Comment:"快发大师打包工具"
Name:"{group}\快发大师"; Filename: "{app}\AppLoader.exe";IconFilename:{app}\auto_package.ico
[Tasks]
Name: desktopicon; Description: "创建桌面快捷方式"
[Code]
procedure InitializeWizard(); 
begin
WizardForm.WelcomeLabel1.Font.Color:= clNavy; 
WizardForm.WelcomeLabel2.Font.Color:= clTeal; 
end;
