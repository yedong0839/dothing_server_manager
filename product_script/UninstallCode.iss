; -- UninstallCodeExample1.iss --
;
; This script shows various things you can achieve using a [Code] section for Uninstall

[Setup]
AppName=DOTHING服务管理器
AppVersion=1.5
;DefaultDirName={pf}\dothing
DefaultDirName=D:\dothing
DefaultGroupName=DOTHING服务管理器
;UninstallDisplayIcon={app}\MyProg.exe
OutputDir=output
SetupIconFile=D:\tools\dothing\dothing_server_manager.ico
Uninstallable=yes
UninstallDisplayName=卸载DOTHING服务管理器
[Files]
;Source: "MyProg.exe"; DestDir: "{app}"
;Source: "MyProg.chm"; DestDir: "{app}"
Source: "D:\tools\dothing\dothing_server_manager.exe"; DestDir: "{app}"
Source: "D:\tools\dothing\init.exe"; DestDir: "{app}"Source: "D:\tools\dothing\*"; DestDir: "{app}"; Flags: recursesubdirs
Source: "D:\tools\dothing\Readme.txt"; DestDir: "{app}"; Flags: isreadmeSource: "D:\tools\dothing\dothing_server_manager.ico"; DestDir: "{app}"
Source: "D:\tools\dothing\vcredist_x86.exe"; DestDir: "{app}"
Source: "D:\tools\dothing\exit.exe"; DestDir: "{app}"
;Source: "D:\tools\dothing\vcredist_x86_2013.exe"; DestDir: "{app}"
[Languages]
Name: "zh"; MessagesFile: "compiler:Languages\Chinese.isl"

[Icons]
Name:"{commondesktop}\DOTHING管理器";Filename:"{app}\dothing_server_manager.exe";Tasks:desktopicon;WorkingDir: "{app}";IconFilename:{app}\dothing_server_manager.ico;Comment:"DOTHING服务管理器"
Name:"{group}\DOTHING管理器"; Filename: "{app}\dothing_server_manager.exe";IconFilename:{app}\dothing_server_manager.ico
[Tasks]
Name: desktopicon; Description: "创建桌面快捷方式"
[Code]
procedure InitializeWizard(); 
begin
WizardForm.WelcomeLabel1.Font.Color:= clNavy; 
WizardForm.WelcomeLabel2.Font.Color:= clTeal; 
end;
[Run]
Filename: "{app}\vcredist_x86.exe"; Flags:skipifdoesntexist runhidden
;Filename: "{app}\vcredist_x86_2013.exe";Flags:skipifdoesntexist runhidden
Filename: "{app}\init.exe"; Description: "注册DOTHING服务"; Flags: nowait postinstall skipifsilent
[UninstallRun]
Filename: "{app}\exit.exe"; Flags: skipifdoesntexist runhidden