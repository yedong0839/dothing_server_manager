import platform
import os
import gw_file_system


def set_env_path():
    if platform.system() == 'Darwin':
        lib_path = gw_file_system.get_tool_path('')

        os.environ['PATH'] = lib_path + ':' + os.environ['PATH']

        if os.path.exists(os.path.join(gw_file_system.get_cur_dir(), '../tool/mac/jre/bin/')):
            lib_path = ':' + os.path.join(gw_file_system.get_cur_dir(), '../tool/mac/jre/bin/')
            os.environ['PATH'] = lib_path + ':' + os.environ['PATH']
        if os.path.exists('/System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Commands/'):
            lib_path = ':' + '/System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Commands/'
            os.environ['PATH'] = lib_path + ':' + os.environ['PATH']

    elif platform.system() == 'Windows':
        lib_path = gw_file_system.get_tool_path('')
        lib_path = lib_path.encode('gbk')
        os.environ['PATH'] = lib_path + ';' + os.environ['PATH']
        jre_path = gw_file_system.get_tool_path('jre/bin')
        jre_path = jre_path.encode('gbk')
        os.environ['PATH'] = jre_path + ';' + os.environ['PATH']
    elif platform.system() == 'Linux':
        lib_path = gw_file_system.get_tool_path('')
        os.environ['PATH'] = lib_path + ':' + os.environ['PATH']


def get_java():
    if platform.system() == 'Darwin':
        java_path = os.path.join(gw_file_system.get_cur_dir(), '../tool/mac/jre/bin/')
        if os.path.exists(java_path):
            return java_path + 'java'
        else:
            # get java form env
            return 'java'
    elif platform.system() == 'Windows':
        java_path = os.path.join(gw_file_system.get_cur_dir(), '../tool/win/jre/bin/')
        if os.path.exists(java_path):
            return java_path + 'java'
        else:
            # get java form env
            return 'java'
    else:
         # get java form env (linux version)
        return 'java'
