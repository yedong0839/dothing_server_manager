#ifndef WEBCONFIG_H
#define WEBCONFIG_H

#include <QDialog>

namespace Ui {
class WebConfig;
}

class WebConfig : public QDialog
{
    Q_OBJECT

public:
    explicit WebConfig(QWidget *parent = 0);
    ~WebConfig();

signals:
	void webConfigSetOverSignal(QString cmd);

private slots:
    void on_pushButtonChooseWebDocument_pressed();

    void on_pushButtonSave_pressed();

    void flushUi();

    void on_pushButtonCheckPort_pressed();

private:
    Ui::WebConfig *ui;
    QString m_curDir;
};

#endif // WEBCONFIG_H
