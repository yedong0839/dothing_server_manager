/********************************************************************************
** Form generated from reading UI file 'WebConfig.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WEBCONFIG_H
#define UI_WEBCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_WebConfig
{
public:
    QFrame *frame;
    QLabel *label;
    QToolButton *close;
    QRadioButton *radioButtonWebPort;
    QLineEdit *lineEditPort;
    QPushButton *pushButtonCheckPort;
    QRadioButton *radioButtonWebDocument;
    QLineEdit *lineEditWebDocument;
    QPushButton *pushButtonChooseWebDocument;
    QPushButton *pushButtonSave;
    QRadioButton *radioButtonPHP55;
    QRadioButton *radioButtonPHP53;
    QRadioButton *radioButtonPHP54;

    void setupUi(QDialog *WebConfig)
    {
        if (WebConfig->objectName().isEmpty())
            WebConfig->setObjectName(QStringLiteral("WebConfig"));
        WebConfig->resize(385, 169);
        WebConfig->setStyleSheet(QLatin1String("#WebConfig{\n"
"	border:1px solid rgb(85, 170, 255);\n"
"}"));
        frame = new QFrame(WebConfig);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(2, 2, 381, 161));
        frame->setStyleSheet(QLatin1String("QLineEdit{\n"
"	border:1px solid #ddd;\n"
"	border-radius:3px;\n"
"}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 381, 31));
        label->setStyleSheet(QString::fromUtf8("background-color: rgb(1, 115, 224);\n"
"color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        close = new QToolButton(frame);
        close->setObjectName(QStringLiteral("close"));
        close->setGeometry(QRect(340, 0, 37, 31));
        close->setCursor(QCursor(Qt::PointingHandCursor));
        close->setStyleSheet(QLatin1String("QToolButton{\n"
"background-color: transparent;\n"
"	color: rgb(255, 255, 255);\n"
"	\n"
"	image: url(:/dothing/res/dialogClose.png);\n"
"border-style:none;\n"
"}\n"
"\n"
"QToolButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"\n"
"QToolButton:hover {\n"
"     background-color: rgb(218,54,16);\n"
"	color: red;\n"
" }"));
        radioButtonWebPort = new QRadioButton(frame);
        radioButtonWebPort->setObjectName(QStringLiteral("radioButtonWebPort"));
        radioButtonWebPort->setGeometry(QRect(10, 50, 111, 16));
        radioButtonWebPort->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonWebPort->setCheckable(false);
        radioButtonWebPort->setChecked(false);
        lineEditPort = new QLineEdit(frame);
        lineEditPort->setObjectName(QStringLiteral("lineEditPort"));
        lineEditPort->setGeometry(QRect(120, 50, 131, 20));
        pushButtonCheckPort = new QPushButton(frame);
        pushButtonCheckPort->setObjectName(QStringLiteral("pushButtonCheckPort"));
        pushButtonCheckPort->setGeometry(QRect(270, 50, 75, 23));
        pushButtonCheckPort->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonCheckPort->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        radioButtonWebDocument = new QRadioButton(frame);
        radioButtonWebDocument->setObjectName(QStringLiteral("radioButtonWebDocument"));
        radioButtonWebDocument->setGeometry(QRect(10, 90, 111, 16));
        radioButtonWebDocument->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonWebDocument->setCheckable(false);
        radioButtonWebDocument->setChecked(false);
        lineEditWebDocument = new QLineEdit(frame);
        lineEditWebDocument->setObjectName(QStringLiteral("lineEditWebDocument"));
        lineEditWebDocument->setGeometry(QRect(120, 90, 131, 20));
        pushButtonChooseWebDocument = new QPushButton(frame);
        pushButtonChooseWebDocument->setObjectName(QStringLiteral("pushButtonChooseWebDocument"));
        pushButtonChooseWebDocument->setGeometry(QRect(270, 90, 75, 23));
        pushButtonChooseWebDocument->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonChooseWebDocument->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        pushButtonSave = new QPushButton(frame);
        pushButtonSave->setObjectName(QStringLiteral("pushButtonSave"));
        pushButtonSave->setGeometry(QRect(270, 130, 75, 23));
        pushButtonSave->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonSave->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(212, 97, 3);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        radioButtonPHP55 = new QRadioButton(frame);
        radioButtonPHP55->setObjectName(QStringLiteral("radioButtonPHP55"));
        radioButtonPHP55->setGeometry(QRect(190, 130, 61, 17));
        radioButtonPHP55->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonPHP55->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonPHP55->setChecked(false);
        radioButtonPHP53 = new QRadioButton(frame);
        radioButtonPHP53->setObjectName(QStringLiteral("radioButtonPHP53"));
        radioButtonPHP53->setGeometry(QRect(20, 130, 61, 17));
        radioButtonPHP53->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonPHP53->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonPHP54 = new QRadioButton(frame);
        radioButtonPHP54->setObjectName(QStringLiteral("radioButtonPHP54"));
        radioButtonPHP54->setGeometry(QRect(100, 130, 61, 17));
        radioButtonPHP54->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonPHP54->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonPHP54->setChecked(true);

        retranslateUi(WebConfig);
        QObject::connect(close, SIGNAL(clicked()), WebConfig, SLOT(close()));

        QMetaObject::connectSlotsByName(WebConfig);
    } // setupUi

    void retranslateUi(QDialog *WebConfig)
    {
        WebConfig->setWindowTitle(QApplication::translate("WebConfig", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("WebConfig", "WEB\346\234\215\345\212\241\345\231\250\350\256\276\347\275\256", Q_NULLPTR));
        close->setText(QString());
        radioButtonWebPort->setText(QApplication::translate("WebConfig", "WEB\347\253\257\345\217\243\350\256\276\347\275\256", Q_NULLPTR));
        pushButtonCheckPort->setText(QApplication::translate("WebConfig", "\347\253\257\345\217\243\346\243\200\346\265\213", Q_NULLPTR));
        radioButtonWebDocument->setText(QApplication::translate("WebConfig", "WEB\346\240\271\347\233\256\345\275\225\350\256\276\347\275\256", Q_NULLPTR));
        pushButtonChooseWebDocument->setText(QApplication::translate("WebConfig", "\351\200\211\346\213\251\347\233\256\345\275\225", Q_NULLPTR));
        pushButtonSave->setText(QApplication::translate("WebConfig", "\344\277\235\345\255\230", Q_NULLPTR));
        radioButtonPHP55->setText(QApplication::translate("WebConfig", "PHP5.5", Q_NULLPTR));
        radioButtonPHP53->setText(QApplication::translate("WebConfig", "PHP5.2", Q_NULLPTR));
        radioButtonPHP54->setText(QApplication::translate("WebConfig", "PHP5.4", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WebConfig: public Ui_WebConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WEBCONFIG_H
