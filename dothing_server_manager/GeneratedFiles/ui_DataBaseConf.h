/********************************************************************************
** Form generated from reading UI file 'DataBaseConf.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATABASECONF_H
#define UI_DATABASECONF_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_DataBaseConf
{
public:
    QFrame *frame;
    QFrame *frame_2;
    QLabel *label;
    QToolButton *close;
    QLineEdit *lineEditLocalhost;
    QLineEdit *lineEditPort;
    QLineEdit *lineEditUserName;
    QLineEdit *lineEditPwd;
    QLineEdit *lineEditDataBase;
    QPushButton *pushButtonTestConnect;
    QPushButton *pushButtonSave;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_5;

    void setupUi(QDialog *DataBaseConf)
    {
        if (DataBaseConf->objectName().isEmpty())
            DataBaseConf->setObjectName(QStringLiteral("DataBaseConf"));
        DataBaseConf->resize(458, 208);
        DataBaseConf->setStyleSheet(QLatin1String("#DataBaseConf{\n"
"	border:1px solid rgb(85, 170, 255);\n"
"}"));
        frame = new QFrame(DataBaseConf);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(3, 3, 451, 201));
        frame->setStyleSheet(QLatin1String("QLineEdit{\n"
"	border:1px solid #ddd;\n"
"	border-radius:3px;\n"
"}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame_2 = new QFrame(frame);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(0, 0, 544, 31));
        frame_2->setStyleSheet(QString::fromUtf8("background-color: rgb(1, 115, 224);\n"
"color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 71, 16));
        close = new QToolButton(frame_2);
        close->setObjectName(QStringLiteral("close"));
        close->setGeometry(QRect(410, 0, 37, 31));
        close->setCursor(QCursor(Qt::PointingHandCursor));
        close->setStyleSheet(QLatin1String("QToolButton{\n"
"background-color: transparent;\n"
"	color: rgb(255, 255, 255);\n"
"	\n"
"	image: url(:/dothing/res/dialogClose.png);\n"
"	border-style:none;\n"
"}\n"
"\n"
"QToolButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"\n"
"QToolButton:hover {\n"
"     background-color: rgb(218,54,16);\n"
"	color: red;\n"
" }"));
        lineEditLocalhost = new QLineEdit(frame);
        lineEditLocalhost->setObjectName(QStringLiteral("lineEditLocalhost"));
        lineEditLocalhost->setGeometry(QRect(90, 40, 113, 20));
        lineEditLocalhost->setStyleSheet(QLatin1String("border:1px solid #ddd;\n"
"border-radius:3px;"));
        lineEditPort = new QLineEdit(frame);
        lineEditPort->setObjectName(QStringLiteral("lineEditPort"));
        lineEditPort->setGeometry(QRect(310, 40, 113, 20));
        lineEditUserName = new QLineEdit(frame);
        lineEditUserName->setObjectName(QStringLiteral("lineEditUserName"));
        lineEditUserName->setGeometry(QRect(90, 80, 113, 20));
        lineEditPwd = new QLineEdit(frame);
        lineEditPwd->setObjectName(QStringLiteral("lineEditPwd"));
        lineEditPwd->setGeometry(QRect(310, 80, 113, 20));
        lineEditPwd->setEchoMode(QLineEdit::Password);
        lineEditDataBase = new QLineEdit(frame);
        lineEditDataBase->setObjectName(QStringLiteral("lineEditDataBase"));
        lineEditDataBase->setGeometry(QRect(90, 120, 113, 20));
        pushButtonTestConnect = new QPushButton(frame);
        pushButtonTestConnect->setObjectName(QStringLiteral("pushButtonTestConnect"));
        pushButtonTestConnect->setGeometry(QRect(180, 160, 75, 23));
        pushButtonTestConnect->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonTestConnect->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        pushButtonSave = new QPushButton(frame);
        pushButtonSave->setObjectName(QStringLiteral("pushButtonSave"));
        pushButtonSave->setGeometry(QRect(290, 160, 75, 23));
        pushButtonSave->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonSave->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(212, 97, 3);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        radioButton = new QRadioButton(frame);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setGeometry(QRect(20, 40, 51, 16));
        radioButton->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButton->setCheckable(false);
        radioButton_2 = new QRadioButton(frame);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));
        radioButton_2->setGeometry(QRect(240, 40, 51, 21));
        radioButton_2->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButton_2->setCheckable(false);
        radioButton_3 = new QRadioButton(frame);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setGeometry(QRect(20, 80, 61, 16));
        radioButton_3->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButton_3->setCheckable(false);
        radioButton_4 = new QRadioButton(frame);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setGeometry(QRect(240, 80, 61, 16));
        radioButton_4->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButton_4->setCheckable(false);
        radioButton_5 = new QRadioButton(frame);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));
        radioButton_5->setGeometry(QRect(20, 120, 61, 16));
        radioButton_5->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButton_5->setCheckable(false);

        retranslateUi(DataBaseConf);
        QObject::connect(close, SIGNAL(clicked()), DataBaseConf, SLOT(close()));

        QMetaObject::connectSlotsByName(DataBaseConf);
    } // setupUi

    void retranslateUi(QDialog *DataBaseConf)
    {
        DataBaseConf->setWindowTitle(QApplication::translate("DataBaseConf", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("DataBaseConf", "\346\225\260\346\215\256\345\272\223\350\256\276\347\275\256", Q_NULLPTR));
        close->setText(QString());
        pushButtonTestConnect->setText(QApplication::translate("DataBaseConf", "\346\265\213\350\257\225\350\277\236\346\216\245", Q_NULLPTR));
        pushButtonSave->setText(QApplication::translate("DataBaseConf", "\344\277\235\345\255\230", Q_NULLPTR));
        radioButton->setText(QApplication::translate("DataBaseConf", "\344\270\273\346\234\272", Q_NULLPTR));
        radioButton_2->setText(QApplication::translate("DataBaseConf", "\347\253\257\345\217\243", Q_NULLPTR));
        radioButton_3->setText(QApplication::translate("DataBaseConf", "\347\224\250\346\210\267\345\220\215", Q_NULLPTR));
        radioButton_4->setText(QApplication::translate("DataBaseConf", "\345\257\206\347\240\201", Q_NULLPTR));
        radioButton_5->setText(QApplication::translate("DataBaseConf", "\346\225\260\346\215\256\345\272\223", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DataBaseConf: public Ui_DataBaseConf {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATABASECONF_H
