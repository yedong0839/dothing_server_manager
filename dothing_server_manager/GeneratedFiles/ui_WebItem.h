/********************************************************************************
** Form generated from reading UI file 'WebItem.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WEBITEM_H
#define UI_WEBITEM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WebItem
{
public:
    QFrame *frameItem;
    QCheckBox *checkBox;
    QPushButton *pushButtonTestPort;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout_3;

    void setupUi(QWidget *WebItem)
    {
        if (WebItem->objectName().isEmpty())
            WebItem->setObjectName(QStringLiteral("WebItem"));
        WebItem->resize(560, 53);
        frameItem = new QFrame(WebItem);
        frameItem->setObjectName(QStringLiteral("frameItem"));
        frameItem->setGeometry(QRect(0, 0, 560, 53));
        frameItem->setStyleSheet(QString::fromUtf8("QLineEdit{\n"
"	border:1px solid #ddd;\n"
"	border-radius:3px;\n"
"}\n"
"QFrame{\n"
"	border-style:none;\n"
"}\n"
"QLabel{\n"
"	font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"	color: #666666;\n"
"}\n"
"MyLineEdit{\n"
"	border:1px solid #ddd;\n"
"	border-radius:3px;\n"
"}"));
        frameItem->setFrameShape(QFrame::StyledPanel);
        frameItem->setFrameShadow(QFrame::Raised);
        checkBox = new QCheckBox(frameItem);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(0, 18, 21, 17));
        checkBox->setCursor(QCursor(Qt::PointingHandCursor));
        checkBox->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        pushButtonTestPort = new QPushButton(frameItem);
        pushButtonTestPort->setObjectName(QStringLiteral("pushButtonTestPort"));
        pushButtonTestPort->setGeometry(QRect(489, 12, 61, 25));
        pushButtonTestPort->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonTestPort->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        horizontalLayoutWidget = new QWidget(frameItem);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(20, 0, 461, 51));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);

        retranslateUi(WebItem);

        QMetaObject::connectSlotsByName(WebItem);
    } // setupUi

    void retranslateUi(QWidget *WebItem)
    {
        WebItem->setWindowTitle(QApplication::translate("WebItem", "Dialog", Q_NULLPTR));
        checkBox->setText(QString());
        pushButtonTestPort->setText(QApplication::translate("WebItem", "\346\265\213\350\257\225\347\253\257\345\217\243", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WebItem: public Ui_WebItem {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WEBITEM_H
