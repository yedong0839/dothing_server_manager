/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFrame *frame;
    QFrame *title;
    QToolButton *min;
    QToolButton *close;
    QLabel *label_11;
    QFrame *content;
    QFrame *content_title;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QPushButton *startALL;
    QFrame *line;
    QFrame *line_2;
    QFrame *line_3;
    QFrame *line_4;
    QFrame *line_5;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QFrame *line_6;
    QCheckBox *checkBox;
    QLabel *label_7;
    QLabel *labelApache;
    QLabel *labelMysql;
    QPushButton *stopALL;
    QPushButton *pushButtonWebSet;
    QPushButton *pushButtonDatabaseSet;
    QGroupBox *groupBox;
    QCheckBox *apache;
    QGroupBox *groupBox_2;
    QCheckBox *mysql;
    QGroupBox *groupBox_3;
    QCheckBox *php;
    QFrame *line_7;
    QFrame *line_8;
    QGroupBox *groupBox_4;
    QRadioButton *radioButtonApacheStart;
    QRadioButton *radioButtonApacheStop;
    QGroupBox *groupBox_5;
    QRadioButton *radioButtonMysqlStart;
    QRadioButton *radioButtonMysqlStop;
    QGroupBox *groupBox_6;
    QRadioButton *radioButtonTaskStart;
    QRadioButton *radioButtonTaskStop;
    QGroupBox *groupBox_7;
    QRadioButton *radioButtonMemcacheStart;
    QRadioButton *radioButtonMemcacheStop;
    QLabel *labelTask;
    QLabel *labelMemcache;
    QPushButton *buttonGotoWeb;
    QFrame *line_10;
    QFrame *line_11;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *labelMemcache_3;
    QLabel *labelMemcache_4;
    QPushButton *pushButtonWebSet_2;
    QGroupBox *groupBox_8;
    QRadioButton *radioButtonMemcacheStart_4;
    QRadioButton *radioButtonMemcacheStop_4;
    QGroupBox *groupBox_9;
    QRadioButton *radioButtonMemcacheStart_3;
    QRadioButton *radioButtonMemcacheStop_3;
    QCheckBox *checkBox_6;
    QLabel *label_16;
    QGroupBox *groupBox_11;
    QRadioButton *radioButtonStart_redis;
    QRadioButton *radioButtonStop_redis;
    QLabel *label_17;
    QFrame *line_12;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(753, 495);
        QIcon icon;
        icon.addFile(QStringLiteral(":/dothing/res/th_logo2.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QLatin1String("#centralWidget{\n"
"	border:1px solid rgb(85, 170, 255);\n"
"}"));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(2, 2, 751, 491));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        title = new QFrame(frame);
        title->setObjectName(QStringLiteral("title"));
        title->setGeometry(QRect(0, 0, 750, 90));
        title->setStyleSheet(QLatin1String("#title{\n"
"	background-color:rgb(1,115,224)\n"
"}"));
        title->setFrameShape(QFrame::StyledPanel);
        title->setFrameShadow(QFrame::Raised);
        min = new QToolButton(title);
        min->setObjectName(QStringLiteral("min"));
        min->setGeometry(QRect(670, 0, 41, 31));
        min->setCursor(QCursor(Qt::PointingHandCursor));
        min->setStyleSheet(QLatin1String("QToolButton{\n"
"	background-color: transparent;\n"
"	/*background-image: url(:/dothing/res/dialogMin.png);*/\n"
"	color: rgb(255, 255, 255);\n"
"	\n"
"	image: url(:/dothing/res/dialogMin.png);\n"
"border-style:none;\n"
"}\n"
"\n"
"QToolButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"\n"
"QToolButton:hover {\n"
"     background-color: rgb(218,54,16);\n"
"	color: red;\n"
" }"));
        close = new QToolButton(title);
        close->setObjectName(QStringLiteral("close"));
        close->setGeometry(QRect(710, 0, 37, 31));
        close->setCursor(QCursor(Qt::PointingHandCursor));
        close->setStyleSheet(QLatin1String("QToolButton{\n"
"background-color: transparent;\n"
"	color: rgb(255, 255, 255);\n"
"	\n"
"	image: url(:/dothing/res/dialogClose.png);\n"
"border-style:none;\n"
"}\n"
"\n"
"QToolButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"\n"
"QToolButton:hover {\n"
"     background-color: rgb(218,54,16);\n"
"	color: red;\n"
" }"));
        label_11 = new QLabel(title);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(50, 20, 561, 51));
        label_11->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"font: 20pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        content = new QFrame(frame);
        content->setObjectName(QStringLiteral("content"));
        content->setGeometry(QRect(0, 90, 750, 500));
        content->setStyleSheet(QLatin1String("#content{\n"
"	background-color:white;\n"
"}"));
        content->setFrameShape(QFrame::StyledPanel);
        content->setFrameShadow(QFrame::Raised);
        content_title = new QFrame(content);
        content_title->setObjectName(QStringLiteral("content_title"));
        content_title->setGeometry(QRect(10, 10, 600, 31));
        content_title->setStyleSheet(QLatin1String("#content_title{\n"
"	background-color:rgb(1,115,224)\n"
"}"));
        content_title->setFrameShape(QFrame::StyledPanel);
        content_title->setFrameShadow(QFrame::Raised);
        label = new QLabel(content_title);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 6, 60, 22));
        label->setStyleSheet(QString::fromUtf8("color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        label_2 = new QLabel(content_title);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(150, 6, 60, 22));
        label_2->setStyleSheet(QString::fromUtf8("color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        label_3 = new QLabel(content_title);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(360, 6, 60, 22));
        label_3->setStyleSheet(QString::fromUtf8("color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        label_8 = new QLabel(content_title);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(300, 30, 60, 22));
        label_8->setStyleSheet(QString::fromUtf8("color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        label_9 = new QLabel(content_title);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(250, 6, 31, 22));
        label_9->setStyleSheet(QString::fromUtf8("color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        label_10 = new QLabel(content_title);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(310, 6, 31, 22));
        label_10->setStyleSheet(QString::fromUtf8("color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        startALL = new QPushButton(content);
        startALL->setObjectName(QStringLiteral("startALL"));
        startALL->setGeometry(QRect(630, 10, 115, 30));
        startALL->setCursor(QCursor(Qt::PointingHandCursor));
        startALL->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color: rgb(80, 166, 12);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }"));
        line = new QFrame(content);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(150, 40, 20, 291));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        line_2 = new QFrame(content);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(20, 70, 581, 16));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        line_3 = new QFrame(content);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(20, 110, 581, 16));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(content);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setGeometry(QRect(20, 150, 581, 16));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);
        line_5 = new QFrame(content);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(240, 40, 20, 291));
        line_5->setFrameShape(QFrame::VLine);
        line_5->setFrameShadow(QFrame::Sunken);
        label_4 = new QLabel(content);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(170, 50, 54, 12));
        label_5 = new QLabel(content);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(170, 90, 81, 16));
        label_6 = new QLabel(content);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(170, 130, 81, 16));
        line_6 = new QFrame(content);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setGeometry(QRect(20, 190, 581, 16));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);
        checkBox = new QCheckBox(content);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(30, 170, 121, 16));
        checkBox->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        label_7 = new QLabel(content);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(170, 170, 81, 16));
        labelApache = new QLabel(content);
        labelApache->setObjectName(QStringLiteral("labelApache"));
        labelApache->setGeometry(QRect(370, 40, 231, 31));
        labelApache->setWordWrap(true);
        labelMysql = new QLabel(content);
        labelMysql->setObjectName(QStringLiteral("labelMysql"));
        labelMysql->setGeometry(QRect(370, 80, 231, 31));
        labelMysql->setWordWrap(true);
        stopALL = new QPushButton(content);
        stopALL->setObjectName(QStringLiteral("stopALL"));
        stopALL->setGeometry(QRect(630, 50, 115, 30));
        stopALL->setCursor(QCursor(Qt::PointingHandCursor));
        stopALL->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color: rgb(227, 20, 36);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }"));
        pushButtonWebSet = new QPushButton(content);
        pushButtonWebSet->setObjectName(QStringLiteral("pushButtonWebSet"));
        pushButtonWebSet->setGeometry(QRect(20, 340, 101, 51));
        pushButtonWebSet->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonWebSet->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        pushButtonDatabaseSet = new QPushButton(content);
        pushButtonDatabaseSet->setObjectName(QStringLiteral("pushButtonDatabaseSet"));
        pushButtonDatabaseSet->setGeometry(QRect(260, 340, 111, 51));
        pushButtonDatabaseSet->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonDatabaseSet->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color: rgb(208, 151, 0);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        pushButtonDatabaseSet->setAutoDefault(true);
        groupBox = new QGroupBox(content);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 40, 120, 31));
        groupBox->setStyleSheet(QStringLiteral("border:none;"));
        apache = new QCheckBox(groupBox);
        apache->setObjectName(QStringLiteral("apache"));
        apache->setGeometry(QRect(10, 10, 89, 16));
        apache->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        groupBox_2 = new QGroupBox(content);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(20, 80, 120, 30));
        groupBox_2->setStyleSheet(QStringLiteral("border:none;"));
        mysql = new QCheckBox(groupBox_2);
        mysql->setObjectName(QStringLiteral("mysql"));
        mysql->setGeometry(QRect(10, 10, 89, 16));
        mysql->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        groupBox_3 = new QGroupBox(content);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(30, 120, 131, 31));
        groupBox_3->setStyleSheet(QStringLiteral("border:none;"));
        php = new QCheckBox(groupBox_3);
        php->setObjectName(QStringLiteral("php"));
        php->setGeometry(QRect(0, 10, 121, 16));
        php->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        line_7 = new QFrame(content);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setGeometry(QRect(360, 40, 20, 291));
        line_7->setFrameShape(QFrame::VLine);
        line_7->setFrameShadow(QFrame::Sunken);
        line_8 = new QFrame(content);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setGeometry(QRect(300, 40, 20, 291));
        line_8->setFrameShape(QFrame::VLine);
        line_8->setFrameShadow(QFrame::Sunken);
        groupBox_4 = new QGroupBox(content);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setGeometry(QRect(250, 40, 120, 31));
        groupBox_4->setStyleSheet(QStringLiteral("border:none;"));
        radioButtonApacheStart = new QRadioButton(groupBox_4);
        radioButtonApacheStart->setObjectName(QStringLiteral("radioButtonApacheStart"));
        radioButtonApacheStart->setGeometry(QRect(20, 10, 16, 16));
        radioButtonApacheStart->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonApacheStart->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        radioButtonApacheStop = new QRadioButton(groupBox_4);
        radioButtonApacheStop->setObjectName(QStringLiteral("radioButtonApacheStop"));
        radioButtonApacheStop->setGeometry(QRect(80, 10, 16, 16));
        radioButtonApacheStop->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonApacheStop->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        groupBox_5 = new QGroupBox(content);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(250, 80, 120, 31));
        groupBox_5->setStyleSheet(QStringLiteral("border:none;"));
        radioButtonMysqlStart = new QRadioButton(groupBox_5);
        radioButtonMysqlStart->setObjectName(QStringLiteral("radioButtonMysqlStart"));
        radioButtonMysqlStart->setGeometry(QRect(20, 10, 16, 16));
        radioButtonMysqlStart->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMysqlStart->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        radioButtonMysqlStop = new QRadioButton(groupBox_5);
        radioButtonMysqlStop->setObjectName(QStringLiteral("radioButtonMysqlStop"));
        radioButtonMysqlStop->setGeometry(QRect(80, 10, 16, 16));
        radioButtonMysqlStop->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMysqlStop->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        groupBox_6 = new QGroupBox(content);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setGeometry(QRect(250, 120, 120, 31));
        groupBox_6->setStyleSheet(QStringLiteral("border:none;"));
        radioButtonTaskStart = new QRadioButton(groupBox_6);
        radioButtonTaskStart->setObjectName(QStringLiteral("radioButtonTaskStart"));
        radioButtonTaskStart->setGeometry(QRect(20, 10, 16, 16));
        radioButtonTaskStart->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonTaskStart->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        radioButtonTaskStop = new QRadioButton(groupBox_6);
        radioButtonTaskStop->setObjectName(QStringLiteral("radioButtonTaskStop"));
        radioButtonTaskStop->setGeometry(QRect(80, 10, 16, 16));
        radioButtonTaskStop->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonTaskStop->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        groupBox_7 = new QGroupBox(content);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        groupBox_7->setGeometry(QRect(250, 160, 120, 31));
        groupBox_7->setStyleSheet(QStringLiteral("border:none;"));
        radioButtonMemcacheStart = new QRadioButton(groupBox_7);
        radioButtonMemcacheStart->setObjectName(QStringLiteral("radioButtonMemcacheStart"));
        radioButtonMemcacheStart->setGeometry(QRect(20, 10, 16, 16));
        radioButtonMemcacheStart->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMemcacheStart->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        radioButtonMemcacheStop = new QRadioButton(groupBox_7);
        radioButtonMemcacheStop->setObjectName(QStringLiteral("radioButtonMemcacheStop"));
        radioButtonMemcacheStop->setGeometry(QRect(80, 10, 16, 16));
        radioButtonMemcacheStop->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMemcacheStop->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        labelTask = new QLabel(content);
        labelTask->setObjectName(QStringLiteral("labelTask"));
        labelTask->setGeometry(QRect(370, 120, 231, 31));
        labelTask->setWordWrap(true);
        labelMemcache = new QLabel(content);
        labelMemcache->setObjectName(QStringLiteral("labelMemcache"));
        labelMemcache->setGeometry(QRect(370, 160, 231, 31));
        labelMemcache->setWordWrap(true);
        buttonGotoWeb = new QPushButton(content);
        buttonGotoWeb->setObjectName(QStringLiteral("buttonGotoWeb"));
        buttonGotoWeb->setGeometry(QRect(630, 350, 115, 30));
        buttonGotoWeb->setCursor(QCursor(Qt::PointingHandCursor));
        buttonGotoWeb->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color: rgb(80, 166, 12);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }"));
        line_10 = new QFrame(content);
        line_10->setObjectName(QStringLiteral("line_10"));
        line_10->setGeometry(QRect(20, 230, 581, 16));
        line_10->setFrameShape(QFrame::HLine);
        line_10->setFrameShadow(QFrame::Sunken);
        line_11 = new QFrame(content);
        line_11->setObjectName(QStringLiteral("line_11"));
        line_11->setGeometry(QRect(20, 280, 581, 16));
        line_11->setFrameShape(QFrame::HLine);
        line_11->setFrameShadow(QFrame::Sunken);
        checkBox_3 = new QCheckBox(content);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(30, 210, 121, 16));
        checkBox_3->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        checkBox_4 = new QCheckBox(content);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(30, 250, 121, 16));
        checkBox_4->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        label_13 = new QLabel(content);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(170, 210, 81, 16));
        label_14 = new QLabel(content);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(170, 250, 81, 16));
        labelMemcache_3 = new QLabel(content);
        labelMemcache_3->setObjectName(QStringLiteral("labelMemcache_3"));
        labelMemcache_3->setGeometry(QRect(370, 200, 231, 31));
        labelMemcache_3->setWordWrap(true);
        labelMemcache_4 = new QLabel(content);
        labelMemcache_4->setObjectName(QStringLiteral("labelMemcache_4"));
        labelMemcache_4->setGeometry(QRect(370, 240, 231, 31));
        labelMemcache_4->setWordWrap(true);
        pushButtonWebSet_2 = new QPushButton(content);
        pushButtonWebSet_2->setObjectName(QStringLiteral("pushButtonWebSet_2"));
        pushButtonWebSet_2->setGeometry(QRect(140, 340, 101, 51));
        pushButtonWebSet_2->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonWebSet_2->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        groupBox_8 = new QGroupBox(content);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        groupBox_8->setGeometry(QRect(250, 240, 120, 41));
        groupBox_8->setStyleSheet(QStringLiteral("border:none;"));
        radioButtonMemcacheStart_4 = new QRadioButton(groupBox_8);
        radioButtonMemcacheStart_4->setObjectName(QStringLiteral("radioButtonMemcacheStart_4"));
        radioButtonMemcacheStart_4->setGeometry(QRect(20, 10, 16, 16));
        radioButtonMemcacheStart_4->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMemcacheStart_4->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        radioButtonMemcacheStop_4 = new QRadioButton(groupBox_8);
        radioButtonMemcacheStop_4->setObjectName(QStringLiteral("radioButtonMemcacheStop_4"));
        radioButtonMemcacheStop_4->setGeometry(QRect(80, 10, 16, 16));
        radioButtonMemcacheStop_4->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMemcacheStop_4->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        groupBox_9 = new QGroupBox(content);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        groupBox_9->setGeometry(QRect(250, 190, 120, 41));
        groupBox_9->setStyleSheet(QStringLiteral("border:none"));
        radioButtonMemcacheStart_3 = new QRadioButton(groupBox_9);
        radioButtonMemcacheStart_3->setObjectName(QStringLiteral("radioButtonMemcacheStart_3"));
        radioButtonMemcacheStart_3->setGeometry(QRect(20, 20, 16, 16));
        radioButtonMemcacheStart_3->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMemcacheStart_3->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        radioButtonMemcacheStop_3 = new QRadioButton(groupBox_9);
        radioButtonMemcacheStop_3->setObjectName(QStringLiteral("radioButtonMemcacheStop_3"));
        radioButtonMemcacheStop_3->setGeometry(QRect(80, 20, 16, 16));
        radioButtonMemcacheStop_3->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonMemcacheStop_3->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        checkBox_6 = new QCheckBox(content);
        checkBox_6->setObjectName(QStringLiteral("checkBox_6"));
        checkBox_6->setGeometry(QRect(30, 300, 121, 16));
        checkBox_6->setStyleSheet(QString::fromUtf8("QCheckBox {\n"
"	spacing: 2px;\n"
"font: 12pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QCheckBox::indicator {\n"
"	width: 20px;\n"
"	height: 20px;\n"
"}\n"
"\n"
"QCheckBox::indicator:unchecked {\n"
"	image: url(:/dothing/res/checkboxUnchecked.png);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	image: url(:/dothing/res/checkboxChecked.png);\n"
"}"));
        label_16 = new QLabel(content);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(170, 300, 81, 16));
        groupBox_11 = new QGroupBox(content);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        groupBox_11->setGeometry(QRect(250, 290, 120, 41));
        groupBox_11->setStyleSheet(QStringLiteral("border:none;"));
        radioButtonStart_redis = new QRadioButton(groupBox_11);
        radioButtonStart_redis->setObjectName(QStringLiteral("radioButtonStart_redis"));
        radioButtonStart_redis->setGeometry(QRect(20, 10, 16, 16));
        radioButtonStart_redis->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonStart_redis->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        radioButtonStop_redis = new QRadioButton(groupBox_11);
        radioButtonStop_redis->setObjectName(QStringLiteral("radioButtonStop_redis"));
        radioButtonStop_redis->setGeometry(QRect(80, 10, 16, 16));
        radioButtonStop_redis->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonStop_redis->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 16px \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: #666666;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	image:url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	image:url(:/dothing/res/radioSelected.png);\n"
"}\n"
""));
        label_17 = new QLabel(content);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(370, 290, 231, 31));
        label_17->setWordWrap(true);
        line_12 = new QFrame(content);
        line_12->setObjectName(QStringLiteral("line_12"));
        line_12->setGeometry(QRect(20, 320, 581, 16));
        line_12->setFrameShape(QFrame::HLine);
        line_12->setFrameShadow(QFrame::Sunken);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);
        QObject::connect(close, SIGNAL(clicked()), MainWindow, SLOT(close()));
        QObject::connect(min, SIGNAL(clicked()), MainWindow, SLOT(hide()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "DOTHING\346\234\215\345\212\241\347\256\241\347\220\206\345\231\250", Q_NULLPTR));
        min->setText(QString());
        close->setText(QString());
        label_11->setText(QApplication::translate("MainWindow", "DOTHING\346\234\215\345\212\241\347\256\241\347\220\206\345\231\250", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "\346\234\215\345\212\241\345\220\215\347\247\260", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "\346\217\217\350\277\260", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "\350\267\257\345\276\204", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "\346\217\217\350\277\260", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "\345\220\257\345\212\250", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "\345\201\234\346\255\242", Q_NULLPTR));
        startALL->setText(QApplication::translate("MainWindow", "\345\205\250\351\203\250\345\220\257\345\212\250", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "WEB\346\234\215\345\212\241", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "\346\225\260\346\215\256\345\272\223\346\234\215\345\212\241", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "\350\256\241\345\210\222\346\234\215\345\212\241", Q_NULLPTR));
        checkBox->setText(QApplication::translate("MainWindow", "MEMCACHE", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "\347\274\223\345\255\230\346\234\215\345\212\241", Q_NULLPTR));
        labelApache->setText(QApplication::translate("MainWindow", "D:/appache.exe", Q_NULLPTR));
        labelMysql->setText(QApplication::translate("MainWindow", "D:/appache.exe", Q_NULLPTR));
        stopALL->setText(QApplication::translate("MainWindow", "\345\205\250\351\203\250\345\201\234\346\255\242", Q_NULLPTR));
        pushButtonWebSet->setText(QApplication::translate("MainWindow", "APACHE\346\234\215\345\212\241\345\231\250\n"
"\350\256\276\347\275\256", Q_NULLPTR));
        pushButtonDatabaseSet->setText(QApplication::translate("MainWindow", "\346\225\260\346\215\256\345\272\223\n"
"\346\234\215\345\212\241\345\231\250\350\256\276\347\275\256", Q_NULLPTR));
        groupBox->setTitle(QString());
        apache->setText(QApplication::translate("MainWindow", "APACHE", Q_NULLPTR));
        groupBox_2->setTitle(QString());
        mysql->setText(QApplication::translate("MainWindow", "MYSQL", Q_NULLPTR));
        groupBox_3->setTitle(QString());
        php->setText(QApplication::translate("MainWindow", "TASK", Q_NULLPTR));
        groupBox_4->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonApacheStart->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\220\257\345\212\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonApacheStart->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonApacheStop->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\205\263\351\227\255", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonApacheStop->setText(QString());
        groupBox_5->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMysqlStart->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\220\257\345\212\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMysqlStart->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMysqlStop->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\205\263\351\227\255", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMysqlStop->setText(QString());
        groupBox_6->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonTaskStart->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\220\257\345\212\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonTaskStart->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonTaskStop->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\205\263\351\227\255", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonTaskStop->setText(QString());
        groupBox_7->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMemcacheStart->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\220\257\345\212\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMemcacheStart->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMemcacheStop->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\205\263\351\227\255", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMemcacheStop->setText(QString());
        labelTask->setText(QApplication::translate("MainWindow", "D:/appache.exe", Q_NULLPTR));
        labelMemcache->setText(QApplication::translate("MainWindow", "D:/appache.exe", Q_NULLPTR));
        buttonGotoWeb->setText(QApplication::translate("MainWindow", "\350\277\233\345\205\245\347\263\273\347\273\237", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("MainWindow", "NGINX", Q_NULLPTR));
        checkBox_4->setText(QApplication::translate("MainWindow", "MYWECHAT", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "WEB\346\234\215\345\212\241", Q_NULLPTR));
        label_14->setText(QApplication::translate("MainWindow", "\345\276\256\344\277\241\346\234\215\345\212\241", Q_NULLPTR));
        labelMemcache_3->setText(QApplication::translate("MainWindow", "D:/appache.exe", Q_NULLPTR));
        labelMemcache_4->setText(QApplication::translate("MainWindow", "D:/appache.exe", Q_NULLPTR));
        pushButtonWebSet_2->setText(QApplication::translate("MainWindow", "NGINX\346\234\215\345\212\241\345\231\250\n"
"\350\256\276\347\275\256", Q_NULLPTR));
        groupBox_8->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMemcacheStart_4->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\220\257\345\212\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMemcacheStart_4->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMemcacheStop_4->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\205\263\351\227\255", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMemcacheStop_4->setText(QString());
        groupBox_9->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMemcacheStart_3->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\220\257\345\212\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMemcacheStart_3->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonMemcacheStop_3->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\205\263\351\227\255", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonMemcacheStop_3->setText(QString());
        checkBox_6->setText(QApplication::translate("MainWindow", "REDIS", Q_NULLPTR));
        label_16->setText(QApplication::translate("MainWindow", "\347\274\223\345\255\230\346\234\215\345\212\241", Q_NULLPTR));
        groupBox_11->setTitle(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonStart_redis->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\220\257\345\212\250", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonStart_redis->setText(QString());
#ifndef QT_NO_TOOLTIP
        radioButtonStop_redis->setToolTip(QApplication::translate("MainWindow", "\345\215\225\345\207\273\345\205\263\351\227\255", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        radioButtonStop_redis->setText(QString());
        label_17->setText(QApplication::translate("MainWindow", "D:/appache.exe", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
