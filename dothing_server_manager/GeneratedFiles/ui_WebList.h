/********************************************************************************
** Form generated from reading UI file 'WebList.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WEBLIST_H
#define UI_WEBLIST_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WebList
{
public:
    QFrame *frame;
    QLabel *label;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QToolButton *close;
    QPushButton *pushButtonAdd;
    QPushButton *pushButtonSave;
    QRadioButton *radioButtonPHP53;
    QRadioButton *radioButtonPHP55;
    QRadioButton *radioButtonPHP54;
    QPushButton *pushButtonDelete;
    QRadioButton *radioButtonPHP71;

    void setupUi(QWidget *WebList)
    {
        if (WebList->objectName().isEmpty())
            WebList->setObjectName(QStringLiteral("WebList"));
        WebList->resize(596, 408);
        WebList->setStyleSheet(QLatin1String("#WebList{\n"
"	border:1px solid rgb(85, 170, 255);\n"
"}"));
        frame = new QFrame(WebList);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(2, 1, 591, 405));
        frame->setStyleSheet(QLatin1String("#frame{\n"
"	border:1px solid rgb(85, 170, 255);\n"
"}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 591, 41));
        label->setStyleSheet(QString::fromUtf8("background-color: rgb(1, 115, 224);\n"
"color:white;\n"
"font: 14px \"\345\276\256\350\275\257\351\233\205\351\273\221\";"));
        scrollArea = new QScrollArea(frame);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setGeometry(QRect(2, 40, 585, 291));
        scrollArea->setStyleSheet(QLatin1String("QScrollArea{\n"
"	/*border:1px solid #ddd;*/\n"
"	border-style:none;\n"
"	\n"
"	background-color: transparent;\n"
"\n"
"}\n"
"\n"
"QScrollBar:vertical\n"
"{\n"
"    width:5px;\n"
"   background:rgba(29,190,214,0%);\n"
"\n"
"    margin:0px,0px,0px,0px;\n"
"    padding-top:9px;   \n"
"    padding-bottom:9px;\n"
"}\n"
"QScrollBar::handle:vertical\n"
"{\n"
"    width:8px;\n"
"    background:rgba(0,0,0,25%);\n"
"	background-color:#1dbed6;\n"
"    border-radius:4px;  \n"
"    min-height:20;\n"
"}\n"
"QScrollBar::handle:vertical:hover\n"
"{\n"
"    width:8px;\n"
"    background:rgba(0,0,0,50%);   \n"
"    border-radius:4px;\n"
"    min-height:20;\n"
"}\n"
"QScrollBar::add-line:vertical   \n"
"{\n"
"    height:9px;width:8px;\n"
"    border-image:url(:/images/a/3.png);\n"
"    subcontrol-position:bottom;\n"
"}\n"
"QScrollBar::sub-line:vertical   \n"
"{\n"
"    height:9px;width:8px;\n"
"    border-image:url(:/images/a/1.png);\n"
"    subcontrol-position:top;\n"
"}\n"
"QScrollBar::add-line:vertical:hover  \n"
"{\n"
"    "
                        "height:9px;width:8px;\n"
"    border-image:url(:/images/a/4.png);\n"
"    subcontrol-position:bottom;\n"
"}\n"
"QScrollBar::sub-line:vertical:hover  \n"
"{\n"
"    height:9px;width:8px;\n"
"    border-image:url(:/images/a/2.png);\n"
"    subcontrol-position:top;\n"
"}\n"
"QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical   \n"
"{\n"
"    background:rgba(0,0,0,10%);\n"
"    border-radius:4px;\n"
"}"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 585, 291));
        scrollAreaWidgetContents->setStyleSheet(QStringLiteral(""));
        scrollArea->setWidget(scrollAreaWidgetContents);
        close = new QToolButton(frame);
        close->setObjectName(QStringLiteral("close"));
        close->setGeometry(QRect(550, 0, 37, 41));
        close->setCursor(QCursor(Qt::PointingHandCursor));
        close->setStyleSheet(QLatin1String("QToolButton{\n"
"background-color: transparent;\n"
"	color: rgb(255, 255, 255);\n"
"	\n"
"	image: url(:/dothing/res/dialogClose.png);\n"
"border-style:none;\n"
"}\n"
"\n"
"QToolButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"\n"
"QToolButton:hover {\n"
"     background-color: rgb(218,54,16);\n"
"	color: red;\n"
" }"));
        pushButtonAdd = new QPushButton(frame);
        pushButtonAdd->setObjectName(QStringLiteral("pushButtonAdd"));
        pushButtonAdd->setGeometry(QRect(130, 370, 80, 25));
        pushButtonAdd->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonAdd->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        pushButtonSave = new QPushButton(frame);
        pushButtonSave->setObjectName(QStringLiteral("pushButtonSave"));
        pushButtonSave->setGeometry(QRect(240, 370, 50, 25));
        pushButtonSave->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonSave->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color:rgb(1, 115, 224);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        radioButtonPHP53 = new QRadioButton(frame);
        radioButtonPHP53->setObjectName(QStringLiteral("radioButtonPHP53"));
        radioButtonPHP53->setGeometry(QRect(130, 340, 61, 17));
        radioButtonPHP53->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonPHP53->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonPHP55 = new QRadioButton(frame);
        radioButtonPHP55->setObjectName(QStringLiteral("radioButtonPHP55"));
        radioButtonPHP55->setGeometry(QRect(320, 340, 61, 17));
        radioButtonPHP55->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonPHP55->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonPHP55->setChecked(false);
        radioButtonPHP54 = new QRadioButton(frame);
        radioButtonPHP54->setObjectName(QStringLiteral("radioButtonPHP54"));
        radioButtonPHP54->setGeometry(QRect(230, 340, 61, 17));
        radioButtonPHP54->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonPHP54->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonPHP54->setChecked(true);
        pushButtonDelete = new QPushButton(frame);
        pushButtonDelete->setObjectName(QStringLiteral("pushButtonDelete"));
        pushButtonDelete->setGeometry(QRect(330, 370, 50, 25));
        pushButtonDelete->setCursor(QCursor(Qt::PointingHandCursor));
        pushButtonDelete->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color: rgb(255, 255, 255);\n"
"background-color: rgb(227, 20, 36);\n"
"border-radius:5px;\n"
"\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"	color: black;\n"
" }\n"
"QPushButton:hover {\n"
"     background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                       stop: 0 #dadbde, stop: 1 #0066CC);\n"
"	color: black;\n"
" }\n"
""));
        radioButtonPHP71 = new QRadioButton(frame);
        radioButtonPHP71->setObjectName(QStringLiteral("radioButtonPHP71"));
        radioButtonPHP71->setGeometry(QRect(410, 340, 61, 17));
        radioButtonPHP71->setCursor(QCursor(Qt::PointingHandCursor));
        radioButtonPHP71->setStyleSheet(QString::fromUtf8("QRadioButton {\n"
"	spacing: 2px;\n"
"font: 10pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"color:#0b8879;\n"
"}\n"
"\n"
"QRadioButton::indicator {\n"
"	width: 15px; \n"
"	height: 15px; \n"
"}\n"
"\n"
"QRadioButton::indicator::unchecked {\n"
"	\n"
"	image: url(:/dothing/res/radioNormal.png);\n"
"}\n"
"\n"
"QRadioButton::indicator::checked {\n"
"	\n"
"	image: url(:/dothing/res/radioSelected.png);\n"
"}"));
        radioButtonPHP71->setChecked(false);
        scrollArea->raise();
        label->raise();
        close->raise();
        pushButtonAdd->raise();
        pushButtonSave->raise();
        radioButtonPHP53->raise();
        radioButtonPHP55->raise();
        radioButtonPHP54->raise();
        pushButtonDelete->raise();
        radioButtonPHP71->raise();

        retranslateUi(WebList);
        QObject::connect(close, SIGNAL(clicked()), WebList, SLOT(close()));

        QMetaObject::connectSlotsByName(WebList);
    } // setupUi

    void retranslateUi(QWidget *WebList)
    {
        WebList->setWindowTitle(QApplication::translate("WebList", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("WebList", "    WEB\346\234\215\345\212\241\345\231\250\350\256\276\347\275\256", Q_NULLPTR));
        close->setText(QString());
        pushButtonAdd->setText(QApplication::translate("WebList", "\346\226\260\345\242\236\344\270\200\350\241\214", Q_NULLPTR));
        pushButtonSave->setText(QApplication::translate("WebList", "\344\277\235\345\255\230", Q_NULLPTR));
        radioButtonPHP53->setText(QApplication::translate("WebList", "PHP5.2", Q_NULLPTR));
        radioButtonPHP55->setText(QApplication::translate("WebList", "PHP5.5", Q_NULLPTR));
        radioButtonPHP54->setText(QApplication::translate("WebList", "PHP5.4", Q_NULLPTR));
        pushButtonDelete->setText(QApplication::translate("WebList", "\345\210\240\351\231\244", Q_NULLPTR));
        radioButtonPHP71->setText(QApplication::translate("WebList", "PHP7.1", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WebList: public Ui_WebList {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WEBLIST_H
