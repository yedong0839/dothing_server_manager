#ifndef WEBLIST_H
#define WEBLIST_H
#include <QWidget>
#include <QGridLayout>
#include <QPropertyAnimation>
#include "WebItem.h"

namespace Ui {
	class WebList;
};

class WebList : public QWidget
{
	Q_OBJECT
public:
	explicit WebList();
	~WebList();

private:
	Ui::WebList *ui;
	QString m_virtualHost;
	QGridLayout *m_mainLayout;
	QString m_curDir;
	QPropertyAnimation *m_animation;
	WebItem *m_selectedItem;
	void clearLayout();
	void getPHP();
	void setApacheConfigParam();
	QString getVirtualHost(QString port, QString docRoot, QString phpIncludePath);

signals:
	void flushUISignal();
	void webConfigSetOverSignal(QString cmd);

	private slots:
	void setList();
	void add();	
	void save();
	void deleteItem();
	void selectItem(int itemRow);
};
#endif