#include "WebItem.h"
#include "ui_WebItem.h"
#include "MyLineEdit.h"
#include <QLabel>
#include <QFileDialog>
#include <QTcpServer>
#include <QMessageBox>

#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif

WebItem::WebItem(QString port, QString documentRoot, QString phpPath,QString checkBoxName, int itemRow) :QWidget(0), ui(new Ui::WebItem)
{
	ui->setupUi(this);
	setFixedSize(560,50);
	m_itemRow = itemRow;

	QLabel* labelWebPort = new QLabel("端口", this);
	webPort = new MyLineEdit(this);
	webPort->setText(port);
	webPort->setFixedSize(40,30);
	QLabel* labelWebDocRoot = new QLabel("Web根目录", this);
	docRoot = new MyLineEdit(this);
	docRoot->setFixedHeight(30);
	docRoot->setText(documentRoot);
	QLabel* labelPhpIncludePath = new QLabel("php包含目录", this);
	phpIncludePath = new MyLineEdit(this);
	phpIncludePath->setFixedHeight(30);
	phpIncludePath->setText(phpPath);

	ui->horizontalLayout_3->addWidget(labelWebPort);
	ui->horizontalLayout_3->addWidget(webPort);
	ui->horizontalLayout_3->addWidget(labelWebDocRoot);
	ui->horizontalLayout_3->addWidget(docRoot);
	ui->horizontalLayout_3->addWidget(labelPhpIncludePath);
	ui->horizontalLayout_3->addWidget(phpIncludePath);

	ui->checkBox->setObjectName(checkBoxName);

	connect(ui->checkBox,SIGNAL(clicked(bool)),this,SLOT(checkBoxClickSLot(bool)));
	connect(ui->pushButtonTestPort, SIGNAL(clicked()),this,SLOT(checkPort()));
	connect(docRoot,SIGNAL(clicked()),this,SLOT(setDocRoot()));
	connect(phpIncludePath, SIGNAL(clicked()), this, SLOT(setPhpIncludePath()));
}

WebItem::~WebItem()
{
	webPort->deleteLater();
	docRoot->deleteLater();
	phpIncludePath->deleteLater();
	delete ui;
}
bool WebItem::getCheckStatus()
{
	bool checked = ui->checkBox->isChecked();
	return checked;
}

QString WebItem::getCheckName()
{
	return ui->checkBox->objectName();
}

QString WebItem::getPort()
{
	return webPort->text().trimmed();

}

QString WebItem::getDocumentRoot()
{
	return docRoot->text().trimmed();
}

QString WebItem::getPHPIncludePath()
{
	return phpIncludePath->text().trimmed();
}

void WebItem::checkBoxClickSLot(bool click)
{

	emit checkBoxClickSignal(m_itemRow);

}

void WebItem::setDocRoot()
{
	QString flag = QStringLiteral("Web Document Root");
	QString default;
	default = docRoot->text();
	default == "" ? "/" : default;
	QString dir = QFileDialog::getExistingDirectory(this, flag,
		default,
		QFileDialog::ShowDirsOnly
		| QFileDialog::DontResolveSymlinks);

	if (dir == "") return;

	docRoot->setText(dir);
}

void WebItem::setPhpIncludePath()
{
	QString flag = QStringLiteral("PHP Include Path");
	QString default;
	default = phpIncludePath->text();
	default == "" ? "/" : default;
	QString dir = QFileDialog::getExistingDirectory(this, flag,
		default,
		QFileDialog::ShowDirsOnly
		| QFileDialog::DontResolveSymlinks);

	if (dir == "") return;

	phpIncludePath->setText(dir);
}

void WebItem::checkPort()
{

	QString port = webPort->text();
	QTcpServer *pServer = new QTcpServer(this);
	if (pServer->listen(QHostAddress::Any, port.toUInt()))
	{
		QMessageBox::information(NULL, "提示", "端口可以使用");

	}
	else
	{
		QMessageBox::warning(NULL, "提示", "端口已经被占用");
	}
	pServer->deleteLater();
}