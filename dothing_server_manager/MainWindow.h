#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QPainter>
#include <QCheckBox>
#include <QSystemTrayIcon>
#include <QAction>
#include <QMenu>
#include <QProcess>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QString m_curDir;
	QString m_restartCmd;
	QProcess *m_redis_process;
    QMap<int,QString> m_service;

	void startService(int serviceType);
	void stopService(int serviceType);

	void flushUi();

    QPoint move_point; //移动的距离
    bool mouse_press; //鼠标按下
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

	//托盘
	QSystemTrayIcon *trayIcon;
	QAction *minimizeAction;
	QAction *restoreAction;
	QAction *quitAction;
	QMenu   *trayIconMenu;

	void serviceOperate(QString cmd, int serviceType);

	/*start redis*/
	void startRedis();
	void stopRedis();

protected:
    virtual void paintEvent(QPaintEvent *event);

private slots:
    void on_pushButtonWebSet_pressed();
	void on_pushButtonWebSet_2_pressed();
	void on_pushButtonDatabaseSet_pressed();
    void on_radioButtonApacheStart_clicked(bool checked);
    void on_radioButtonApacheStop_clicked(bool checked);
    void on_radioButtonMysqlStart_clicked(bool checked);
    void on_radioButtonMysqlStop_clicked(bool checked);
    void on_radioButtonTaskStart_clicked(bool checked);
    void on_radioButtonTaskStop_clicked(bool checked);
    void on_startALL_pressed();
    void on_stopALL_pressed();
	void goIntoWebSysterm();

	//执行完毕
	void endLoading();
	//重启第一步 停止
	void stopRe(QString cmd);
	//重启第二步 启动
	void startRe();
	//重启nginx
	void restartNginx(QString cmd);
	//
	void finishRedisSlot();
	//托盘
	void trayiconActivated(QSystemTrayIcon::ActivationReason reason);

	/*各按钮的slot*/
    void on_radioButtonMemcacheStart_clicked(bool checked);
    void on_radioButtonMemcacheStop_clicked(bool checked);
	void on_radioButtonMemcacheStart_3_clicked(bool checked);
	void on_radioButtonMemcacheStop_3_clicked(bool checked);
	void on_radioButtonMemcacheStart_4_clicked(bool checked);
	void on_radioButtonMemcacheStop_4_clicked(bool checked);
	void on_radioButtonStart_redis_clicked(bool checkd);
	void on_radioButtonStop_redis_clicked(bool checkd);

};

enum ServiceType{
    APACHE,
    MYSQL,
    TASK,
	MEMCACHE,
	WECHAT,
	NGINX
};
#endif // MAINWINDOW_H