#ifndef WEBITEM_H
#define WEBITEM_H
#include <QWidget>
#include <QGridLayout>
#include "MyLineEdit.h"

namespace Ui {
	class WebItem;
};

class WebItem :public QWidget
{
	Q_OBJECT
public:
	explicit WebItem(QString port, QString documentRoot, QString phpPath, QString checkBoxName, int itemRow);
	~WebItem();
	bool getCheckStatus();
	QString getCheckName();
	QString getPort();
	QString getDocumentRoot();
	QString getPHPIncludePath();

private:
	Ui::WebItem *ui;
	int m_itemRow;
	MyLineEdit *phpIncludePath;
	MyLineEdit *webPort;
	MyLineEdit *docRoot;

signals:
	void checkBoxClickSignal(int itemRow);

	private slots:
	void checkBoxClickSLot(bool click);
	void setDocRoot();
	void setPhpIncludePath();
	void checkPort();
};

#endif