﻿#include "DataLoaderDialog.h"

DataLoaderDialog::DataLoaderDialog()
{
    this->setFixedSize(125, 125);
	this->setWindowModality(Qt::ApplicationModal);//设置窗体模态，要求该窗体没有父类，否则无效

    int width = this->width();

    // 透明度
    this->setWindowOpacity(0.7);

    // 背景色
	//this->setStyleSheet("background-color: rgb(0, 0, 0);");
	this->setStyleSheet("background-color: transparent;");

    // 去标题
	this->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    
    m_notifyLabel = new QLabel(this);

    m_notifyLabel->setStyleSheet("background-color: transparent;");

    m_movie = new QMovie(":/dothing/res/loadingimg.gif");

    m_notifyLabel->setScaledContents(true);

    m_notifyLabel->setMovie(m_movie);

    m_movie->start();
}

DataLoaderDialog::~DataLoaderDialog(){
	m_movie->deleteLater();
	m_notifyLabel->deleteLater();
}

DataLoaderDialog* DataLoaderDialog::getInstance()
{
    static DataLoaderDialog dlg;
    return &dlg;
}