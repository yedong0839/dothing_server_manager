#-------------------------------------------------
#
# Project created by QtCreator 2015-07-28T16:22:56
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dothing_server_manager
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    WebConfig.cpp \
    DataBaseConf.cpp \
    Service.cpp

HEADERS  += MainWindow.h \
    WebConfig.h \
    DataBaseConf.h \
    Service.h

FORMS    += MainWindow.ui \
    WebConfig.ui \
    DataBaseConf.ui

RESOURCES += \
    dothing.qrc
RC_FILE = icon.rc
