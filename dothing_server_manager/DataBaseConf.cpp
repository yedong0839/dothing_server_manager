﻿#include "DataBaseConf.h"
#include "ui_DataBaseConf.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QSettings>
#include <QSqlError>

#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif

DataBaseConf::DataBaseConf(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataBaseConf)
{
    setWindowFlags(Qt::FramelessWindowHint);
    setWindowModality(Qt::ApplicationModal);
    setAttribute(Qt::WA_DeleteOnClose);

    ui->setupUi(this);
    flushUI();
}

DataBaseConf::~DataBaseConf()
{
    delete ui;
}

void DataBaseConf::flushUI(){
    QSettings settings("data.ini", QSettings::IniFormat);

    QString server = settings.value("data/server","localhost").toString();
    QString port = settings.value("data/port","3336").toString();
    QString userName = settings.value("data/userName","root").toString();
    QString pwd = settings.value("data/pwd","myoa888").toString();
    QString database = settings.value("data/database","dothing").toString();


    ui->lineEditPort->setText(port);
    ui->lineEditLocalhost->setText(server);
    ui->lineEditUserName->setText(userName);
    ui->lineEditPwd->setText(pwd);
    ui->lineEditDataBase->setText(database);
}

void DataBaseConf::on_pushButtonSave_pressed()
{
    QString port = ui->lineEditPort->text();
    QString server = ui->lineEditLocalhost->text();
    QString userName = ui->lineEditUserName->text();
    QString pwd = ui->lineEditPwd->text();
    QString database = ui->lineEditDataBase->text();

    if(port == "" || server == "" || userName == "" || pwd == "" || database == ""){
        QMessageBox::warning(NULL,"提示","所有参数不能为空");
        return;
    }
    bool isNumber;
    port.toInt(&isNumber);
    if (!isNumber){
        QMessageBox::warning(NULL,"提示","端口只能是数字");
        return;
    }

    QSettings settings("data.ini", QSettings::IniFormat); // 当前目录的INI文件
    settings.beginGroup("data");
    settings.setValue("server", server);
    settings.setValue("port", port);
    settings.setValue("userName", userName);
    settings.setValue("pwd", pwd);
    settings.setValue("database", database);
    settings.endGroup();

	QSettings settingsD("server/mysql/bin/my.ini", QSettings::IniFormat); // 当前目录的INI文件
	settingsD.beginGroup("mysqld");
	settingsD.setValue("port", port);
	settingsD.endGroup();

	settingsD.beginGroup("client");
	settingsD.setValue("port", port);
	settingsD.endGroup();

	if (QMessageBox::Ok == QMessageBox::information(NULL, "提示", "保存成功")){
		close();
	}
}

void DataBaseConf::on_pushButtonTestConnect_pressed()
{
	
	QString port = ui->lineEditPort->text();
	QString server = ui->lineEditLocalhost->text();
	QString userName = ui->lineEditUserName->text();
	QString pwd = ui->lineEditPwd->text();
	QString database = ui->lineEditDataBase->text();

	if (QSqlDatabase::contains("qt_sql_default_connection")){
		db = QSqlDatabase::database("qt_sql_default_connection");
	}
	else{
		db = QSqlDatabase::addDatabase("QMYSQL"); //添加数据库驱动
	}
	
	db.setHostName(server);
	db.setPort(port.toUInt());
	db.setDatabaseName(database);
	db.setUserName(userName);
	db.setPassword(pwd);

	if (!db.open())
	{
		
		QMessageBox::warning(NULL, "连接失败", db.lastError().text());
	}
	else
	{
		QMessageBox::information(NULL, "提示", "连接成功");
	}
	db.close();
}
