﻿#ifndef _DATA_LOADER_DIALOG_H_
#define _DATA_LOADER_DIALOG_H_

#include <QtCore>
#include <QDialog>
#include <QLabel>
#include <QMovie>
#include <QProgressDialog>

class DataLoaderDialog : public QDialog
{
public:
    DataLoaderDialog();
    ~DataLoaderDialog();
    static DataLoaderDialog* getInstance();
private:
    QLabel* m_notifyLabel;
    QMovie* m_movie;
};
/*
class UploadFileDialog : public QProgressDialog
{
public:
    UploadFileDialog();
    ~UploadFileDialog();
};*/
#endif