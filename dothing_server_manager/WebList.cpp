#include "WebList.h"
#include "ui_WebList.h"
#include <QDebug>
#include <QSettings>
#include <QList>
#include <QStringList>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QFileDialog>

#if _MSC_VER >= 1600
#pragma execution_character_set("utf-8")
#endif

WebList::WebList() : QWidget(0), ui(new Ui::WebList), m_selectedItem(NULL)
{
	ui->setupUi(this);
	setWindowFlags(Qt::FramelessWindowHint);
	setWindowModality(Qt::ApplicationModal);
	setAttribute(Qt::WA_DeleteOnClose);

	m_mainLayout = new QGridLayout();
	m_mainLayout->setSizeConstraint(QLayout::SetMinimumSize);
	m_mainLayout->setAlignment(Qt::AlignTop);
	m_mainLayout->setVerticalSpacing(5);

	ui->scrollAreaWidgetContents->adjustSize();
	connect(this,SIGNAL(flushUISignal()),this,SLOT(setList()));
	connect(ui->pushButtonAdd,SIGNAL(clicked()),this,SLOT(add()));
	connect(ui->pushButtonDelete, SIGNAL(clicked()), this, SLOT(deleteItem()));	
	connect(ui->pushButtonSave, SIGNAL(clicked()), this, SLOT(save()));


#ifdef _DEBUG
	m_curDir = "D:/tools/dothing_server_manager";
#else
	m_curDir = QCoreApplication::applicationDirPath();
#endif

	emit flushUISignal();
	getPHP();

	//窗口动画
	m_animation = new QPropertyAnimation(this, "windowOpacity", this);
	m_animation->setDuration(500);
	m_animation->setStartValue(0);
	m_animation->setEndValue(1);
	m_animation->start();

}

WebList::~WebList()
{
	clearLayout();
	m_mainLayout->deleteLater();
	m_animation->deleteLater();
	delete ui;
}

void WebList::setList()
{
	clearLayout();

	QSettings settings("virtualhost.ini", QSettings::IniFormat);

	m_virtualHost = settings.value("num/web").toString();

	int size = m_virtualHost.toInt();
	QString port;
	QString documentRoot;
	QString phpPath;
	QString keyPort;
	QString keyDocumentRoot;
	QString keyPhpPath;

	int itemRow = 0;
	for (int i = 1; i <=size; i++)
	{
		keyPort = "num" + QString::number(i) + "/port";
		keyDocumentRoot = "num" + QString::number(i) + "/documentRoot";
		keyPhpPath = "num" + QString::number(i) + "/phpIncludePath";

		port = settings.value(keyPort).toString();
		documentRoot = settings.value(keyDocumentRoot).toString();
		phpPath = settings.value(keyPhpPath).toString();

		itemRow = i - 1;
		WebItem* item = new WebItem(port, documentRoot, phpPath,"num" + QString::number(i), itemRow);
		m_mainLayout->addWidget(item,itemRow,0);
		connect(item, SIGNAL(checkBoxClickSignal(int)),this,SLOT(selectItem(int)));
	}
	ui->scrollAreaWidgetContents->setLayout(m_mainLayout);
	ui->scrollAreaWidgetContents->adjustSize();
}

void WebList::clearLayout()
{
	QLayoutItem* item;
	while ((item = m_mainLayout->takeAt(0)) != 0)
	{
		m_mainLayout->removeItem(item);
		delete item;
	}
}

void WebList::add()
{
	QSettings settings("virtualhost.ini", QSettings::IniFormat); // 当前目录的INI文件
	int size = m_virtualHost.toInt();
	size++;
	QString key = "num" + QString::number(size);
	settings.beginGroup(key);
	settings.setValue("port", "");
	settings.setValue("documentRoot", "");
	settings.setValue("phpPath", "");
	settings.endGroup();

	settings.beginGroup("num");
	settings.setValue("web", QString::number(size));
	settings.endGroup();

	setList();
}

void WebList::deleteItem()
{
	QLayoutItem* item;
	WebItem *webItem;
	QString key;
	bool checkFlag = false;

	QSettings settings("virtualhost.ini", QSettings::IniFormat); // 当前目录的INI文件
	int size = m_virtualHost.toInt();
	while (item = m_mainLayout->takeAt(0))
	{
		webItem = dynamic_cast<WebItem*>(item->widget());
		if (webItem->getCheckStatus()){
			checkFlag = true;
			key = webItem->getCheckName();
			settings.remove(key);
			size--;
		}
	}

	if (!checkFlag){
		QMessageBox::information(NULL, "提示", "请选择一项");
		return;
	}

	settings.setValue("num/web", size);

	QSettings settingsNew("virtualhost.ini", QSettings::IniFormat); // 当前目录的INI文件
	QString keyNum;
	int sizeOld = m_virtualHost.toInt();
	QString port,documentRoot,phpIncludePath;
	QList<QStringList>numList;

	for (int i = 1; i <= sizeOld; i++)
	{
		keyNum = "num" + QString::number(i);
		port = settings.value(keyNum + "/port").toString();
		documentRoot = settings.value(keyNum + "/documentRoot").toString();
		phpIncludePath = settings.value(keyNum + "/phpIncludePath").toString();

		if (port.isEmpty()){
			settingsNew.remove(keyNum);
			continue;
		}
		QStringList content;
		content<< port << documentRoot<<phpIncludePath;
		numList.push_back(content);
		settingsNew.remove(keyNum);
	}

	QFile file("virtualhost.ini");
	file.remove();

	QSettings settingsNewSave("virtualhost.ini", QSettings::IniFormat); // 当前目录的INI文件
	QStringList stringListNew;
	QString virtualHostConfig = "";
	for (int i = 1; i <= numList.size(); i++)
	{
		stringListNew = numList.at(i - 1);
		keyNum = "num" + QString::number(i);
		settingsNewSave.setValue(keyNum + "/port", stringListNew.at(0));
		settingsNewSave.setValue(keyNum + "/documentRoot", stringListNew.at(1));
		settings.setValue(keyNum + "/phpIncludePath", stringListNew.at(2));

		virtualHostConfig += getVirtualHost(stringListNew.at(0), stringListNew.at(1), stringListNew.at(2));
	}
	settingsNewSave.setValue("num/web", numList.size());

	QString apacheRoot = m_curDir + "/server/apache";
	QFile confFile(apacheRoot + "/conf/extra/httpd-vhosts.conf");
	if (confFile.open(QIODevice::WriteOnly)){
		confFile.write(virtualHostConfig.toLatin1());
		confFile.flush();
		confFile.close();
	}
	else{
		qDebug() << "打开" + apacheRoot + "/conf/extra/httpd-vhosts.conf" + "文件失败";
		return;
	}

	QMessageBox msgBox;
	msgBox.setText("删除成功");
	msgBox.setInformativeText("web服务将会重启");
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();

	if (QMessageBox::Ok == ret){
		QString cmd = "net stop DOTHING_WEB_SERVER";
		emit webConfigSetOverSignal(cmd);
		close();
	}
	
}

void WebList::save()
{
	QFile file("virtualhost.ini");
	file.remove();

	QLayoutItem* item;
	WebItem *webItem;
	QString keyNum;
	QString port;
	QString documentRoot;
	QString phpIncludePath;
	QString virtualHostConfig = "";
	QSettings settings("virtualhost.ini", QSettings::IniFormat);

	int number = 1;
	while (item = m_mainLayout->takeAt(0))
	{
		webItem = dynamic_cast<WebItem*>(item->widget());
		port = webItem->getPort();
		if (port.isEmpty()){
			continue;
		}
		documentRoot = webItem->getDocumentRoot();
		phpIncludePath = webItem->getPHPIncludePath();

		keyNum = "num" + QString::number(number);
		settings.setValue(keyNum + "/port", port);
		settings.setValue(keyNum + "/documentRoot", documentRoot);
		settings.setValue(keyNum + "/phpIncludePath", phpIncludePath);

		number++;

		virtualHostConfig +=getVirtualHost(port,documentRoot,phpIncludePath);
	}

	settings.setValue("num/web", --number);

	QString apacheRoot = m_curDir + "/server/apache";
	QFile confFile(apacheRoot + "/conf/extra/httpd-vhosts.conf");
	if (confFile.open(QIODevice::WriteOnly)){
		confFile.write(virtualHostConfig.toLatin1());
		confFile.flush();
		confFile.close();
	}
	else{
		qDebug() << "打开" + apacheRoot + "/conf/extra/httpd-vhosts.conf" + "文件失败";
		return;
	}

	setApacheConfigParam();

	QMessageBox msgBox;
	msgBox.setText("保存成功");
	msgBox.setInformativeText("web服务将会重启");
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();

	if (QMessageBox::Ok == ret){
		QString cmd = "net stop DOTHING_WEB_SERVER";
		emit webConfigSetOverSignal(cmd);
		close();
	}

}

void WebList::getPHP()
{
	QSettings settings("web.ini", QSettings::IniFormat);
	QString php = settings.value("web/defaultphp").toString();

	if (php == "53"){
		ui->radioButtonPHP53->setChecked(true);
	}
	else if (php == "54"){
		ui->radioButtonPHP54->setChecked(true);
	}
	else if (php == "55") {
		ui->radioButtonPHP55->setChecked(true);
	}
	else if (php == "71") {
		ui->radioButtonPHP71->setChecked(true);
	}
	else
	{
		
	}

}

void WebList::setApacheConfigParam()
{
	QString phpIniFilePath;
	QString defaultPHPIncludePath;
	QString apache_conf;
	apache_conf = "ServerRoot \"%1\"\r\n";
	//apache_conf += "DocumentRoot \"%2\"\r\n";
	//apache_conf += "<Directory \"%3\">\r\n";
	//apache_conf += "Options Indexes FollowSymLinks Includes ExecCGI\r\n";
	//apache_conf += "AllowOverride All\r\n";
	//apache_conf += "Require all granted\r\n";
	//apache_conf += "</Directory>\r\n";
	//apache_conf += "ServerName localhost:%4\r\n";
	//apache_conf += "Listen %5\r\n";

	QSettings settings("web.ini", QSettings::IniFormat); // 当前目录的INI文件
	settings.beginGroup("web");
	//settings.setValue("documentRoot", documentRoot);
	//settings.setValue("port", port);

	if (ui->radioButtonPHP53->isChecked()){
		settings.setValue("defaultphp", 53);
		apache_conf += " Include \"conf/extra/httpd-dothing-pre.conf\"";
		phpIniFilePath = m_curDir + "/server/pre/php/php.ini";
		defaultPHPIncludePath = m_curDir + "/server/pre/php/PEAR";
	}
	else if (ui->radioButtonPHP54->isChecked()){
		settings.setValue("defaultphp", 54);
		apache_conf += " Include \"conf/extra/httpd-dothing-pre54.conf\"";
		phpIniFilePath = m_curDir + "/server/pre/php54/php.ini";
		defaultPHPIncludePath = m_curDir + "/server/pre/php54/PEAR";
	}
	else if (ui->radioButtonPHP71->isChecked()) {
		settings.setValue("defaultphp", 71);
		apache_conf += " Include \"conf/extra/httpd-dothing-pre71.conf\"";
		phpIniFilePath = m_curDir + "/server/pre/php71/php.ini";
		defaultPHPIncludePath = m_curDir + "/server/pre/php71/PEAR";
	}
	else
	{
		settings.setValue("defaultphp", 55);
		apache_conf += " Include \"conf/extra/httpd-dothing.conf\"";
		phpIniFilePath = m_curDir + "/server/php/php.ini";
		defaultPHPIncludePath = m_curDir + "/server/php/PEAR";
	}
	/*
	if (!setPHPini(phpIniFilePath, documentRoot, defaultPHPIncludePath)){
		if (QMessageBox::Ok == QMessageBox::information(NULL, "提示", "修改PHPini文件失败")){
			close();
		}
		return;
	}*/

	QString apacheRoot = m_curDir + "/server/apache";
	//apache_conf = apache_conf.arg(apacheRoot).arg(documentRoot).arg(documentRoot).arg(port).arg(port);
	apache_conf = apache_conf.arg(apacheRoot);

	QFile confFile(apacheRoot + "/conf/extra/param.conf");
	if (confFile.open(QIODevice::WriteOnly)){
		confFile.write(apache_conf.toLatin1());
		confFile.flush();
		confFile.close();
	}
	else{
		qDebug() << "打开" + apacheRoot + "/conf/extra/param.conf" + "文件失败";
		return;
	}

	settings.endGroup();
}

void WebList::selectItem(int itemRow)
{
	QLayoutItem* item;
	WebItem *webItem;

	item = m_mainLayout->itemAtPosition(itemRow,0);
	webItem = dynamic_cast<WebItem*>(item->widget());

	if (webItem->getCheckStatus()){
		m_selectedItem = webItem;
	}
	else
	{
		m_selectedItem = NULL;
	}

}
QString WebList::getVirtualHost(QString port,QString docRoot,QString phpIncludePath)
{
	QString virtualHostConfig;
	virtualHostConfig = "Listen %1\r\n";
	virtualHostConfig += "<VirtualHost *:%2>\r\n";
	virtualHostConfig += "DocumentRoot \"%3\"\r\n";
	virtualHostConfig += "<Directory \"%4\">\r\n";
	virtualHostConfig += "Options Indexes FollowSymLinks Includes ExecCGI\r\n";
	virtualHostConfig += "AllowOverride All\r\n";
	virtualHostConfig += "Require all granted\r\n";
	virtualHostConfig += "</Directory>\r\n";
	virtualHostConfig += "php_value  include_path \".;%5\"\r\n";
	virtualHostConfig += "</VirtualHost>\r\n";

	virtualHostConfig = virtualHostConfig.arg(port).arg(port).arg(docRoot).arg(docRoot).arg(phpIncludePath);

	return virtualHostConfig;

}
/*
bool setPHPini(QString phpIniPath, QString webroot, QString defaultIncludePath)
{
	QFile phpIniFile(phpIniPath);
	if (!phpIniFile.exists()){
		qDebug() << phpIniPath << "does't exist";
		return false;
	}

	if (!phpIniFile.open(QIODevice::ReadWrite)){
		qDebug() << "open php ini file fail";
		return false;
	}
	QString contentReplace;
	QString phpInclude;
	QSettings settings("web.ini", QSettings::IniFormat);
	QString phpIncludePath = settings.value("web/phpIncludePath").toString();
	contentReplace = defaultIncludePath;
	if (!phpIncludePath.isEmpty()){
		contentReplace = phpIncludePath;
	}
	qDebug() << "content replace:" << contentReplace;
	QByteArray content = phpIniFile.readAll();
	QString contentString = content;
	phpInclude = webroot;
	contentString.replace(contentReplace, phpInclude);
	//qDebug() << contentString;
	//phpIniFile.write(contentString.toLatin1());
	phpIniFile.flush();
	phpIniFile.close();

	phpIniFile.remove();

	QFile phpIniFileTarget(phpIniPath);

	if (!phpIniFileTarget.open(QIODevice::ReadWrite)){
		qDebug() << "open php ini file fail";
		return false;
	}

	phpIniFileTarget.write(contentString.toLatin1());
	phpIniFileTarget.flush();
	phpIniFileTarget.close();

	settings.setValue("web/phpIncludePath", phpInclude);
	return true;
}
*/