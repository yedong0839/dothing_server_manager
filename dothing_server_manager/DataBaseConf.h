#ifndef DATABASECONF_H
#define DATABASECONF_H
#include <QSqlDatabase>
#include <QDialog>

namespace Ui {
class DataBaseConf;
}

class DataBaseConf : public QDialog
{
    Q_OBJECT

public:
    explicit DataBaseConf(QWidget *parent = 0);
    ~DataBaseConf();
    void flushUI();

private slots:
    void on_pushButtonSave_pressed();

    void on_pushButtonTestConnect_pressed();

private:
    Ui::DataBaseConf *ui;
	QSqlDatabase db;
};

#endif // DATABASECONF_H
