#ifndef DB_H
#define DB_H
#include <QtSql/QSql>
#include <Qtsql/QSqlQuery>

//struct DBSetting
//{
//public:
//    QString server;
//    QString port;
//    QString userName;
//    QString pwd;
//    QString database;
//};

class DB
{
public:
    DB();
    ~DB();
    static DB* getInstance();
//    static DBSetting dbSetting;
//    static QString m_server;
//    static DBSetting getDbSetting();
    static void setDbSetting();
    QSqlQuery getQuery();
private:
    QSqlDatabase db;

};



#endif // DB_H
