#include "Service.h"
//#include "winuser.h"

Service::Service()
{
}

bool Service::getStatus(std::wstring serverName){
    //LPCWSTR ServiceName = L"DOTHING_WEB_SERVER";
	LPCWSTR ServiceName = serverName.c_str();

    SC_HANDLE hScManager = OpenSCManager(0, // local computer or add computer name here
                                                                         0, // SERVICES_ACTIVE_DATABASE database is opened by default.
                                                                         GENERIC_READ
						                       ); // onyl read info
	if (NULL != hScManager)
	{
        SC_HANDLE hSvc = OpenService(hScManager, // service manager
                                    ServiceName, // service name
                                    GENERIC_READ); // onyl read info
		if (NULL != hSvc)
        {
            SERVICE_STATUS_PROCESS sInfo;

            DWORD bytesNeeded = 0;
			DWORD dwOldCheckPoint;
			DWORD dwStartTickCount;
			DWORD dwWaitTime;
			DWORD dwBytesNeeded;
            if(QueryServiceStatusEx(hSvc,                   // A handle to the service.
                                    SC_STATUS_PROCESS_INFO, // info requested
                                    (LPBYTE)&sInfo,                 // structure to load info to
                                    sizeof(sInfo),          // size of the buffer
                                    &bytesNeeded))
            {
                if(sInfo.dwCurrentState == SERVICE_RUNNING)
                {
					std::cout << "running";
					CloseServiceHandle(hSvc);
					CloseServiceHandle(hScManager);
					return true;
                }
                else
                {
					std::cout << "stopping\n";
					CloseServiceHandle(hSvc);
					CloseServiceHandle(hScManager);
					return false;
                }

            }
			

        }
		CloseServiceHandle(hSvc);
		
    }
	CloseServiceHandle(hScManager);
    return false;
}
